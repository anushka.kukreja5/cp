#include <iostream>
#include<vector>
#include <cmath>

//a function to calculate the next step
int NextStep(int pointer , int value,int size) {
    int result  = (pointer + value) % size;
    if(result < 0) {
        result += size;
    }
    return result;
}

//A function to detect a cycle doesnt exist
bool IsNotCycle(std::vector<int>& nums, bool prev_direction,int pointer) {
    int size = nums.size();
    bool curr_direction = nums[pointer] >= 0;
    if (prev_direction != curr_direction || abs(nums[pointer]%size) == 0) {
        return true;
    }else {
        return false;
    }
}

bool CircularArrayLoop(std::vector<int>& nums) {
    int size = nums.size();

    for(int i = 0; i < size; i++) {
        int slow = i; fast = i;
        bool forward = nums[i] > 0;
        while(true) {
            slow= NextStep(slow,nums[slow],size);

            if(IsNotCycle(nums,forward,slow)) {
                break;
            }

            fast = NextStep(fast,nums[fast],size);
            if(IsNotCycle(nums,forward,fast)) {
                break;
            }

            if(slow == fast) {
                return true;
            }
        }
    }
    return false;

}

int main() {
    std::vector<std::vector<int>> input = {
        {-2,-3,-9},
        {-5,-4,-3,-2,-1},
        {-1,-2,-3,-4,-5},
        {2,1,-1,-2},
        {-1,-2,-3,-4,-5,6},
        {1,2,-3,3,4,7,1},
        {2,2,2,7,2,-1,2,-1,1}
    };
    for(int i=0;i<input.size();i++) {
        std::cout<< i +1 << ".\tGiven arr: " << PrintArray(input[i]) << "\n";
        bool res = CircularArrayLoop(input[i]);
        std::cout << "\n\tFound loop: " << std::boolalpha << res << "\n";
        std::cout << std::string(100,'-') << "\n";
    }
}
