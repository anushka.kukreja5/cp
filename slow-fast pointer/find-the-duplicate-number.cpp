using namespace std;
int FindDuplicate(vector<int> nums) {
        int fast = nums[0];
        int slow = nums[0];

        while(true) {

            slow= nums[slow];
            fast = nums[nums[fast]];
            if(slow == fast) {
                break;
            }
        }
        slow = nums[0];

        while(slow != fast) {
            slow = nums[slow];
            fast = nums[fast];
        }
        return fast;
}
int main() {
    vector<vector<int>> nums = {
    {1,3,2,3,5,4},
    {2,4,5,4,1,3},
    {1,6,3,5,1,2,7,4},
    {1,2,2,4,3},
    {3,1,3,5,6,4,2}
    };

    for(int i = 0;i<nums.size();i++) {
        string input = "";
        for(int x: nums[i]) {
            input += to_string(x) + ", ";
        }

        input = input.substr(0,input.size()-2);
        cout<< i +1 << ".\tnums: [" << input <<"]" << endl;
        cout << "\tDuplicate Number: " << FindDuplicate(nums[i]) << endl;
        cout << string(100,'-') << endl;
    }
}
