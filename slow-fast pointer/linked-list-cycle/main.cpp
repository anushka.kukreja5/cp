#include<iostream>
#include<string>
#include<vector>

#include "LinkedListNode.cpp"
#include "LinkedList.cpp"
#include "PrintList.cpp"
bool DetectCycle(LinkedListNode* head) {
    if(head == NULL){
     return false;
    }

    LinkedListNode* slow = head;
    LinkedListNode* fast = head;

    while(fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;

        if(slow == fast) {
            return true;
        }
    }
    return false;

}

int main() {
    std::vector<std::vector<int>> inputs = {
        {2,4,6,8,10,12},
        {1,3,5,7,9,11},
        {0,1,2,3,4,6},
        {3,4,7,9,11,17},
        {5,1,4,9,2,3}
    };
    std::vector<int> pos = {0,-1,1,-1,2};

    for(int i = 0; i <inputs.size(); i++) {
        SLLinkedList* linkedlists = new SLLinkedList();
        linkedlists->CreateLinkedList(inputs[i]);
        std::cout<< i + 1 << ".\tInput:\t";
        if(pos[i] == -1) {
            PrintListWithForwardArrow(linkedlists->head);
        }
        else {
            PrintListsWithForwardArrowLoop(linkedlists->head);
        }
        std::cout<<"\n\tpos: "<<pos[i];
        if(pos[i] != -1){
            int length = linkedlists->GetLength(linkedlists->head);
            LinkedListNode* lastNode = linkedlists->GetNode(linkedlists->head,length-1);
            lastNode->next = linkedlists->GetNode(linkedlists->head,pos[i]);
        }
        bool cycle = DetectCycle(linkedlists->head);
        std::cout<< "\n\n\tDetected cycle: " << std::boolalpha << cycle << "\n";
        std::cout <<std::string(100,'-') << "\n";

    }
}
