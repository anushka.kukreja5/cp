struct LinkedListNode {
    int data;
    LinkedListNode *next;
//linkedListNode() will be used to make  a LinkedListNode type object
    LinkedListNode(int d) {
        data = d;
        next = nullptr;
    }
};
