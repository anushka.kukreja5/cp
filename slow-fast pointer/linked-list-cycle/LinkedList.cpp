#include "LinkedListNode.cpp"

class SLLinkedList {
public:
    LinkedListNode* head;
    SLLinkedList() { head = nullptr; }


    void InsertNodeAtHead(LinkedListNode *node) {
        if(head != nullptr) {
            node -> next = head;
        }else {
            head = node;
        }
    }

    void CreateLinkedList(std::vector<int>& vec) {
        for(int i = vec.size() - 1; i>= 0; i--) {
            LinkedListNode *node = new LinkedListNode(vec[i]);
            InsertNodeAtHead(node);
        }
    }

    int GetLength(LinkedListNode* h){
        LinkedListNode *temp = h;
        int length = 0;
        while(temp!=nullptr) {
            length+=1;
            temp = temp->next;
        }

        return length;
    }


    LinkedListNode* GetNode(LinkedListNode* h,int pos) {
        if(pos != -1){
            int p = 0;
            LinkedListNode* ptr = h;
            while(p <pos){
                ptr = ptr->next;
                p += 1;
            }
            return ptr;
        }
        return h;
       }


       std::string ToString() {
        std::string result ="";
        LinkedListNode *temp = head;
        while(temp != nullptr){
            result += std::to_string(temp->data);
            temp = temp->next;
            if(temp != nullptr) {
                result += ", ";
            }
        }
        result += "";
        return result;
       }
};
