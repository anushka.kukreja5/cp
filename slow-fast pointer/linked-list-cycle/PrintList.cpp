#include <iostream>

void PrintListWithForwardArrow(LinkedListNode *linkedListNode) {
    LinkedListNode *temp = linkedListNode;
    while(temp) {
        std::cout << temp->data << " "; //print node value
        temp = temp->next;
        if(temp)
            std::cout << "->" ;
        else//if this is the last node print null at end
            std::cout << "-> NULL ";

    }

}

void PrintListWithForwardArrowLoop(LinkedListNode *linkedListNode) {
    LinkedListNode *temp = linkedListNode;
    while(temp) {
        std::cout << temp->data << " "; //print node value
        temp = temp->next;
        if(temp)
            std::cout << "->" ;
    }

}
