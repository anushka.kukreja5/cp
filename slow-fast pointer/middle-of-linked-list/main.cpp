#include "LinkedList.cpp"
#include "PrintList.cpp"

LinkedListNode* GetMiddleNode(LinkedListNode* head) {
    LinkedListNode* slow = head;
    LinkedListNode* fast = head;

    while(fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
    }

    return slow;

}
int main() {
    std::vector<std::vector<int>> inputs = {
        {1,2,3,4,5},
        {1,2,3,4,5,6},
        {3,2,1},
        {10},
        {1,2}
    };


    for(int i = 0; i <inputs.size(); i++) {

        SLLinkedList* linkedlists = new SLLinkedList();
        linkedlists->CreateSLLinkedList(inputs[i]);

        std::cout << i + 1 <<".\tLinked list: ";
        PrintListWithForwardArrow(linkedlists->head);

        LinkedListNode* middle = GetMiddleNode(linkedlists->head);
        std::cout << "\n\tMiddle of the linked list: " << middle->data << "\n";
        std::cout << std:string(100,'-') << "\n";
    }
}
