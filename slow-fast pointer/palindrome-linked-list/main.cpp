#include "LinkedListReverse.cpp"

bool CompareTwoHalves(LinkedListNode* firstHalf, LinkedListNode* secondHalf) {
    while(firstHalf != nullptr && secondHalf != nullptr) {
        if(firstHalf->data != secondHalf->data) {
            return false;
        }else {
            firstHalf = firstHalf->next;
            secondHalf =secondHalf->next;
        }
    }
    return true;

}
bool Palindrome(LinkedListNode* head) {
    LinkedListNode* slow = head;
    LinkedListNode* fast = head;

    while(fast != nullptr and fast->next != nullptr) {
        slow = slow->next;
        fast = fast->next->next;
    }

    LinkedListNode* revertData = ReversedList(slow);
    bool check = CompareTwoHalves(head,revertData);
    ReversedList(revertData);

    if(check) {
        return true;
    }
    return false;

}
int main() {
    vector<vector<int>> inputs = {
    }
    for(int i = 0; i< inputs.size();i++) {
        SLLinkedList* linkedlists= new SLLinkedList();
        linkedlists->CreateLinkedList(inputs[i]);

        cout<< i +1 << ".\tLinked List: ";
        PrintListWithForwardArrow(linkedlists->head);

        cout << "\n\tIs it a palindrome?: " << (Palindrome(linkedlists->head) ==  true ? "Yes" : "No" )<< "\n";
        cout << string(100,'-') << "\n";
    }
}
