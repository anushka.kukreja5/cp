#include "LinkedList.cpp"
#include "PrintList.cpp"

LinkedListNode* ReversedList(LinkedListNode* slowPtr) {
    LinkedListNode* prev = nullptr;
    LinkedListNode* next = nullptr;
    LinkedListNode* curr = slowPtr;
    while(curr != nullptr) {
        next = curr->next;
        curr->next = prev;
        prev = curr;
        curr = next;
    }
    return prev;
}
