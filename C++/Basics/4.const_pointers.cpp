#include <iostream>
#include<typeinfo> //typeid

using namespace std;

int main()
{
    int v = 10;
    const int a = 20;
    int *const v_const_pointer = &v; //constant pointer of integer type
    //cout<<typeid(pointer_const_v).name()<<endl;
    const int * pointer_const_v  = &v; //pointer of constant integer v

    const int * ptr_const_a = &a; //pointer of constant integer a

    //int *const const_ptr_a  = &a; // creates error kyuki pointer is constant but value is referred which is not constant as ref change toh value change
    std::cout <<"v: "<< v << std::endl;
    //Output: 10
    std::cout << "v_const_pointer: " << v_const_pointer << std::endl;
    //Output: Memory location of v
}

