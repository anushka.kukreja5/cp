#include <iostream>
int main()
{
  const int const_v = 10;
  //int& const_ref = const_v; //creates error - creating an int reference but referring a constant
  const int& const_ref = const_v;

  std::cout << const_v << std::endl;
  //Output: 10
}


