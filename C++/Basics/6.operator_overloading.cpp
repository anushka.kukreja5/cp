#include<iostream>
using namespace std;

class Point {
private:
    int x;
    int y;
public:
    Point() { //Default Constructor
        x = y = 0;
    }
    Point(int x, int y) { //parameterised constructor
        this->x = x; // 'this' is not a keyword in C++. It is a pointer.
        this->y = y;
    }
    /*
    Point(int x, int y): x(x), y(y) {
        //body of code
        //braces mandatory
    }*/
    /*
    The below code is known as Operator Overloading
    */
    Point operator+(const Point &other) {
        Point new_point;
        new_point.x = x + other.x;
        new_point.y = y + other.y;
        return new_point;
    }
    //'operator' is a keyword
    //Overloading the operator '+' so whenever Point+Point is performed then this method will be called.

    friend ostream& operator<<(ostream& outs, Point& point);
    //friend keyword here defines that whatever method is declared after it will be allowed to access private members of that Object/Class whether that method is inside or outside the class but the friend statement has to be declared inside the class.
}; // end of class

//returning an ostream ref object
//overloading << which takes ostream& (here cout) and a Point&
ostream& operator<<(ostream& outs, Point& point) {
    //this is a toString for Point class (v imp)
  outs<<"x = " << point.x << ", y = " << point.y;
  return outs;
}
int main() {
    Point first(10, 20);

    Point second(5, 5);
    Point sum = first + second;
    cout << sum << endl;
    //here only '<< sum' goes to overloaded method

    Point sum1 = first.operator+(second);
    cout << "Another way of calling overloading operator: " << sum1 << endl;
    return 0;
}

/*
class A {
    friend functionWhichAccessPrivateOfAAsWellAsB
};

class B {
    friend  functionWhichAccessPrivateOfAAsWellAsB
};

functionWhichAccessPrivateOfAAsWellAsB() {

}
*/





