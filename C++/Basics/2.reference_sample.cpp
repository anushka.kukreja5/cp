#include<iostream>

void justTimepass(const int& local_reference) {
    //if local reference was 'const int' and not 'const int&' then the address of local_reference & first_variable would be different as it creates new memory location for location_reference and makes a new vaariable.
    //& takes the reference of the argument
    std::cout << "Inside Function"<<std::endl;
    std::cout << "Value of local_reference: " << local_reference << std::endl;
    std::cout << "Address of local_reference: " << &local_reference << std::endl;

    //local_reference = 200;
    //make the arg a constant if the value is not to be changed inside the function as we are dealing with direct memory ie: we have direct access to first_variable/ local_reference

    //when value is not to be changed then use reference
    //RW chahiye toh const mat banao sirf ref banao
    //sirf R chahiye toh const banao

}

int main() {
    int first_variable = 10; //first_variable is name of memory location where 10 is placed.
    int &ref_name = first_variable; //ref_name is pointing where first_variable is pointing
    //ref_name is another name/ alias  to first_variable

    std::cout << "Value of first_variable: " << first_variable << std::endl;
    std::cout << "Value of ref_name: " << ref_name << std::endl;
    std::cout << "Address of first_variable: " << &first_variable << std::endl;
    std::cout << "Address of ref_name: " << &ref_name << std::endl;

    //int second_variable = 20;
    //ref_name = second_variable;
    //here we changed first_variable via ref_name

    // Lets call the function and see what happens
    justTimepass(first_variable);
    std::cout << "Value of first_variable: " << first_variable << std::endl;
    return 0;
}
