

#include <iostream>
int global_var = 1000;

int main()
{
    int global_var = 100;
    std::cout << "Global: "<< ::global_var << std::endl; //can access global variable using ::
    std::cout << "Local: " << global_var << std::endl;
}
