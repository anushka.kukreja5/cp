#include<iostream>
#include<typeinfo> //typeid
#include<string>

int main() {
    int foo;
    auto foo2 = foo;
    int bar = 10;
    auto sum = 0;
    float price = 5.3, cost = 10.1;
    auto val = 5.6;
    auto var = val;
    int a= 0, b[] = {1,1,2}, c(0); //var(value)

    //auto priya = '10.46'; //808334390

//    int priya[] = {1, 2.45, 67.89};
    //true = 1
    //false = 0
    //NULL = 0
//    std::cout<<priya<<std::endl;
    //can also check typeid of any datatype like typeid(datatype)
   // std::cout<<typeid(priya).name()<<std::endl; // ::scope resolution operator
    return 0;
}


//datatype var/obj = {Initialiser List}
//Everything is defined in std namespace. So either we need to use it "using namespace std" or preceed cout with std as std::cout

/*
datatype            typeid
int                     i
char                    c
auto         assigned value type
float                   f
double                  d
bool                    b
null                    x
auto = "Some String"   PKc
void                    v
size_t                  y
wchar_t                 w
*/

//lossy conversion - java
//narrowing conversion - c++
