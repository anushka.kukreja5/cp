#include<iostream>
#include<string>
#include<regex>

using namespace std;

int main() {
    string str("This is the large string containing some numbers like 408 and (382), it also contains some phone numbers like (921) 523-1101, 117-332-1019 and +11314560987. These should all match, but ignore our lone numbers like 433-0983 and (281)2221 since those are not phone numbers");

    cmatch matcher;
    //similar to matcher class in java

    regex allPhoneNumbers(R"delim((\+\d{1,3})?[\.\-\(]*([0-9]{3})[\.\-\)\(\ ]*([0-9]{3})[\.\-\)\(\ ]*([0-9]{4}))delim");
    //delim - deliminate  (to limit something) we can declare delim as anything before any regex like (here) it is delim

    while(regex_search(str.c_str(), matcher, allPhoneNumbers, regex_constants::match_default)) {
        cout<<"Found Match: "<<endl;
        cout<<"["<<matcher[0]<<"]"<<endl;

        str = matcher.suffix().str();
    }
    return 0;
}
