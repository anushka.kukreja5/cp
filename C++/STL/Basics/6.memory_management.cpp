#include<iostream>
#include<memory> //this has unique_ptr

using namespace std;

class Widget {
    int num;
public:
    Widget(int num) : num(num) {
        cout<<"Constructing Widget = "<<num<<endl;
    }
    ~Widget() {
        cout<<"Destroying Widget = " << num<< endl;
    }
};

int main() {
    Widget *w = new Widget(10);
    //new keyword returns pointer to memory where widget obj is created
    //use the 'w' pointer as much as you want
    delete w; //if we dont do this then the memory wont be de-allocated resulting in memory leak.
    //thus destructor will not be called for w.

    //now using unique_ptr
    unique_ptr<Widget> uniqueWidget = make_unique<Widget>(20);
    unique_ptr<Widget> uniqueWidgetCopy = uniqueWidget;
    //shared_ptr is used only for Read acccess - similar to unique_ptr. cannot change once assigned.

    shared_ptr<Widget> sharedWidget = make_shared<Widget>(25);
    sharedWidget = make_shared<Widget>(24);

//shared will always be called after unique ?

    return 0;
}
