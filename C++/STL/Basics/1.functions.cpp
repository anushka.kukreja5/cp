#include<iostream>

class myFunctorClass {
private:
    int _x;
public:
    myFunctorClass(int x) :_x(x) {} //constructor where  _x = x;
    //can declare constructor or destructor like this but braces are mandatory;
    int operator() (int y) {
        std::cout << "Functor called!"<<std::endl;
        return _x + y;
    }
};

int main() {
    //() is overloaded operator which makes it appear as if a function is called
    //classname     objName
    myFunctorClass addFive(5); //Object creation and giving call to parameterised constructor
    //here addFive is a functor
    //while obj creation we declare the mandatory function args
    //when we use it we provide the remaining args
    std::cout <<addFive(6)<<std::endl;
    return 0;
}
