#include<iostream>
#include<string>
#include<typeinfo>
using namespace std;

int main() {
    //Creating String
    string name = "Himanshu Thakkar";
    string name2("Himanshu Thakkar");
    string name3(name);

    string name4 {'H','i','m','a','n','s','h','u'};
    string lastName(name, 9, 7);// 9th index se 7 character
    string line(20, '-'); //line(n times, print this)

    cout<<name<<endl;
    cout<<name2<<endl;
    cout<<name3<<endl;
    cout<<name4<<endl;
    cout<<lastName<<endl;
    cout<<line<<endl;

    //Finding Strings
    size_t pos = name.find(lastName);
    if(pos != string::npos) { //ie at the end
        //string::npos is a constant having value -1
        cout<<"Found  "<<lastName<< " at: "<<pos<<endl;
    }

    //" " in C++ are char*
    cout<<"Comma at: "<<("Hello, How are you?"s).find(",")<<endl;
    //find() takes string
    cout<<line<<endl;

    //Append to String
    string about = "is a Programmer!";
    name.push_back(' ');
    //push at the end
    for(char c: about) {
        name.push_back(c);
    }
    cout<<name<<endl;
    cout<<line<<endl;

    //Insert into String
    name.insert(0, "I have heard that "); //insert(index to insert from, text to insert)
    cout<<name<<endl;
    cout<<line<<endl;

    //Replace some string
    pos = name.find("Programmer");
    name.replace(pos, ("Programmer"s).length(), "teacher");
    //string.replace(from this index, remove this no. of chars, to insert this)

    //Deleting a part of the String
    pos = name.find("heard");
    name.erase(pos, ("heard"s).length());
    name.insert(pos, "read somewhere");
    cout<<name<<endl;

    return 0;
}
//very rare times (in multithreading) c++ asks for start & end point like java
//mostly it asks for start point & num of chars from start.
