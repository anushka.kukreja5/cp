#include<iostream>
using namespace std;

template<typename X> //this fn will only be called when both args are of same type
bool isGreater(X x1, X x2) {
    return x1 > x2;
}

//We can also create specialised template for specific data-type

template<> //this does not make any difference
bool isGreater(int x1, int x2) {//this will only be called when both args are int
    cout<<"Specialied function for int"<<endl;
    return x1 > x2;
}

int main() {

    cout<<std::boolalpha; //setting the flag, to display 0 as 'false' and 1 as 'true'
    cout<<isGreater(5,10)<<endl; //non-templated fn called
    cout<<isGreater(14.46, 8.56)<<endl;
    cout<<isGreater("ZZZ", "AAA")<<endl;
    cout<<isGreater('c','r')<<endl;
}
