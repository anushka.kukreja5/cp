#include<iostream>
using namespace std;

/*
Syntax to define C++ lambda
[](args) {
    //body of lambda
}

Syntax to define templated lambda:

[]<typename T>(T args) {
    //body of templated lambda
}
*/

int main() {
//    cout<<std::boolalpha;
    //if this is not declared then true - 1 & false - 0

//datatype  var  lambda  template    args
    auto isEqual = [] <typename T> (T t1, T t2) {
    //auto as we dont know what is being returned
        return t1 == t2;
    };

    cout<<"Lambda returns: "<<isEqual(10, 10)<<endl;
}
