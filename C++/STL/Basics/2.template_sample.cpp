#include<iostream>
#include<typeinfo>
using namespace std;

template<typename T> //if template like this is defined before a class then that class is generic to defined template.
class Container {
    T t;

public:
    Container(T t) {
        this->t = t;
        //this is a pointer in C++
    }
    //friend if mentioned here or not is fine but it is recomended for inheritance as private members etc will be accessed
    friend ostream& operator <<(std::ostream& os, const Container<T>& c) {
        return (os<<"Container holding: "<<c.t);
    }
};

int main() {
    Container<int> c(100);
    Container<string> s("Some String");

    cout << c << endl;
    cout << s << endl;
    return 0;
}
