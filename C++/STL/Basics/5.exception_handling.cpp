#include<iostream>
#include<exception>
using namespace std;

/*
Syntax to inherit

class NewClass: public OldClass {
    //body of new class
}
*/

class ResourceException: public exception {
    string message;
public:
    ResourceException(const string &msg) :message(msg) { }

    //Java had getmessage() method to get the message of exception but here we have what()
    //return char* pointer as message is returned in char array
    const char* what() const noexcept {
        return message.c_str();
        //c_str() is used to convert string to 'c' style string which is terminated with NULL character '\0'
    }
};
class SomethingImportant {
public:
    SomethingImportant() {
        throw ResourceException("Unable to open some resource I need!"); //can throw custom exception
        //throw 10; //can throw int
        //throw "Hello World"; //can throw char array pointer
    }
};

int main() {
    try {
        SomethingImportant si;
    } catch(exception& e) { //exception& - reference
        cout << e.what() << endl;
        return 1;
    } catch(int i) {
        //can catch integer
        cout<< i << endl;
        return 2;
    } catch(...) {
        //anything general is thrown //general exception handler
        cout<< "CAn catch anything"<<endl;
        return 3;
    }
    return 0;
}
