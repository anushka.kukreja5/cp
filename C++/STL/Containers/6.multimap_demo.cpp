
#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<utility>
#include<map>

using namespace std;

int main() {
    ifstream in ("illiad.txt");//input file stream
    multimap<string, pair<int,int>> wordPositions;
    int lineNumber = 0, wordInLine = 0;
    string line= "";

    while(!in.eof()) {//eof - end of file
        lineNumber++;
        getline(in,line); //cin >> line if issue
        istringstream iss(line);
        string word = "";

        while(!iss.eof()) {
            wordInLine++;
            iss >> word;
            wordPositions.insert(make_pair(word, make_pair(lineNumber, wordInLine)));
        }
        wordInLine = 0;
    }
    cout << "read in " << lineNumber << " lines of text " << endl;

    cout << "size of the wordPositions: " << wordPositions.size() << endl;

    for(auto it = wordPositions.begin(), it2 = it; it!= wordPositions.end(); it = it2) {
        unsigned int count = wordPositions.count(it->first);
        cout << "\"" << it->first << "\" occurs " << count << " times, and is on: \n";

        for(; it2 != wordPositions.end() && it2 -> first == it->first; ++it2) {
            auto [line, word]  = it2 -> second;
            cout << " \tline " << line << ", position " << word << "\n";
        }
    }
    in.close();
    return 0;
}
