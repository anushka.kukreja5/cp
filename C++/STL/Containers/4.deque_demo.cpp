#include<iostream>
#include<deque>
#include<stack>
#include<queue>

using namespace std;

int main() {
    //Deque
    deque<int> transactions;
    //double ended queue - linked list

    transactions.push_front(100);//100
    transactions.push_front(300);//300 100

    transactions.push_back(500);//300 100 500
    transactions.push_back(800);//300 100 500 800

    for(int i=0; i<transactions.size(); i++) {
        cout<<transactions[i] <<" ";
    }
    cout<<endl;

    //Stack
    stack<int> stack1;
    stack1.push(100);
    stack1.push(200);
    stack1.push(300);
    stack1.push(400);

    while(!stack1.empty()) {
        cout<<stack1.top()<< " ";
        stack1.pop();
    }
    cout<<endl;

    //Queue
    queue<int> queue1;
    queue1.push(100);
    queue1.push(200);
    queue1.push(300);
    queue1.push(400);

    while(!queue1.empty()) {
        cout<<queue1.front()<< " ";
        queue1.pop();
    }
    cout<<endl;

    //Priority queue
    //works on stack ie: will pop with highest priority
    //follows descending priority
    priority_queue<int> pqueue;
    pqueue.push(200);
    pqueue.push(600);
    pqueue.push(100);
    pqueue.push(400);
    pqueue.push(40);
    while(!pqueue.empty()) {
        cout<<pqueue.top()<< " ";
        pqueue.pop();
    }
    cout<<endl;

    return 0;
}
