#include<iostream>
#include<vector>
#include<exception>

using namespace std;

template<typename T>
void printVector(const vector<T>& v) {
    cout<<"Size of Vector: "<<v.size()<<endl;//assigned size during initialisation
    cout<<"Capacity of Vector: "<<v.capacity()<<endl; //no. of elements occupying the vector
    cout<<"Max Size of Vector: "<<v.max_size()<<endl;//memory which the vector can occupy in RAM

    for(const T& element: v) {
        cout<<element<<" ";
    }
    cout<<endl<<"==================================="<<endl;
}
int main() {
    vector<int> numbers;
    vector<string> strings = {"Hello", "How", "are", "you", "doing","?"}; //Initialiser List

    numbers.push_back(100);
    numbers.push_back(200);
    numbers.push_back(300);
    numbers.push_back(400);

    cout<<"printing numbers vector: "<<endl;
    printVector(numbers);
    cout<<"printing strings vector: "<<endl;
    printVector(strings);

    //max size of number is more than that of string as string takes more space to occupy one variable naturally


    strings.resize(3);
    cout<<"Printing strings vector after resizing to 3: "<<endl;
    printVector(strings);

    strings.resize(10, "Study Link");
    cout<<"Printing strings vector after resizing to 10 w default value: "<<endl;
    printVector(strings);

    //We can access vector elements using index value also
    numbers[0] = 42;
    numbers.push_back(500);
    numbers.push_back(600);
    cout<<"Printing numbers vector: "<<endl;
    printVector(numbers);

    //Accessing elements of vector using pointers:
    for(int *p = &numbers[0]; p< (&numbers[0] + numbers.size()); p++) {
        cout<<"*p = "<<*p<<"\n";
    }
    //bad way of accessing the elements of vector
    cout<<"This will print some garbage value: "<<numbers[10]<<endl;
    //as there are no restrictions in c++ at runtime (ie no interpreter) an array index can go outof bounds and will return some value ie garbage value.

    //Good way to access the elements of the vector: //using at()
    cout<<"Use this compulsorily in face to face interview: "<<endl;
    try {
        cout<<numbers.at(10);
    } catch(out_of_range& e) {
        cerr<<e.what()<<endl;
    }
    return 0;
}
