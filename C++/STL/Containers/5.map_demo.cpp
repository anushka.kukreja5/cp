
#include<iostream>
#include<map>
using namespace std;

struct SimpleObject {
    int x;
    int y;
    string z;

    SimpleObject() :  x(1), y(2), z("Study Link") { //default constructor

    }
    SimpleObject(int x, int y, string z) : x(x), y(y), z(z) {//param constructor

    }
};
int main() {
    map<string, string> stringMap;
    //  key     value

    stringMap.insert({"Hello", "World"});

    cout<<stringMap["Hello"]<<endl;
    //giving key & value is returned.

    map<string, SimpleObject> objectMap;
    //  key         value

    // Insert elements into map

    // 1. Using 'pair' constructor
    /*
        A pair has two elements and those elements can be accessed with the help of
        'first' and 'second' attribute
    */
    objectMap.insert(std::pair<string, SimpleObject>("first", SimpleObject(4, 2, "CP")));

    // 2. Using [ ] notation or index to add element
    objectMap["second"] = SimpleObject();
    objectMap["second"] = SimpleObject(0, 0, "Overwritten!");// no error thus allows rewriting

    // 3. Using Initializer list which will automatically create pair object for us
    objectMap.insert({"third", SimpleObject(1, 1, "number 3")});

    // insert() returns a pair
    const auto [iterator1, isInserted] = objectMap.insert({"third", SimpleObject()}); // structured binding concept was introduced in C++17
    //structured binding is insert here returning a pair object which will automatically bind first with iterator1 and second with isInserted.

    if(isInserted) {
        cout<<"Item inserted"<<endl;
    } else {
        cout<<"Key already exists, not inserted"<<endl;
    }

    // looping all the elements
    cout<<"Whole Map"<<endl;
    for(auto& x: objectMap) {
        cout << x.first << "->" << x.second.z << endl; // Note: x.second will return the object of SimpleObject
    }

    // deleting from map
    objectMap.erase("second");

    // Checking whether the key is present or not
    auto ptr = objectMap.find("third"); // find returns the pointer to the iterator (so we can access first and second element)

    if(ptr == objectMap.end()) {
        cout << "Unable to find the required key"<<endl;
    }
    else {
        cout << "Found the key: " << ptr->first << ", with value : "<<ptr->second.z << endl;
    }

    // clearing whole map
    objectMap.clear();
    cout << "The objectMap is now size: " << objectMap.size()<<endl;

    return 0;
}
