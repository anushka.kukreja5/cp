#include<iostream>
#include<list>
using namespace std;
int main() {
    list<int> mylist {10, 20, 25, 30, 40, 50};
    list<int> other {25, 35, 150, 45, 50, 55};

    /*merge() transfers all the elements from other to mylist*/
    mylist.merge(other); // sorted list

    cout<<"mylist size: "<<mylist.size()<<endl;
    cout<<"other size: "<<other.size()<<endl;

    /*Following for loop is using iterator (we will study the syntax and all later)*/
    for(auto i=mylist.begin(); i != mylist.end(); i++) {
            //begin is returning iterator
        cout<<*i <<" ";
    }
    cout<<endl;

    mylist.unique(); // sorted list
    for(auto i=mylist.begin(); i != mylist.end(); i++) {
        cout<<*i <<" ";
    }
    cout<<endl;

    mylist.push_front(5); //jagah banake push karega
    for(auto i=mylist.begin(); i != mylist.end(); i++) {
        cout<<*i <<" ";
    }
    cout<<endl;
    return 0;
}

