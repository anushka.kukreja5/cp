#include<iostream>
#include<vector>
#include<list>

using namespace std;

int main() {
    vector<int> vecNum = {1, 2, 3, 4, 5};
    list<int> listNum = {10, 20, 30, 40, 50};

    vector<int>::iterator vit = vecNum.begin();
    list<int>::iterator lit = listNum.begin();

    cout << vit[3] << "\n";
    cout << *(vit + 1) << "\n";

    //Wont  work
    //cout << lit[3] << "\n";
    //cout << *(lit + 1) << "\n";

    //list is doubly linked list cannot access like array

    //Need to do this instead
    std::advance(lit, 3);//advance(base, offset)
    cout << *lit << "\n";

    return 0;
}

