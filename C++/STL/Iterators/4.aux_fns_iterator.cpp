#include <iostream>
#include <iterator>
#include <vector>

using namespace std;

int main() {
    vector<int> numbers {2, 4, 6, 8, 10, 12, 14};
    int arr[]{1, 3, 5, 7, 9, 11, 13};

    for (auto it = begin(numbers); it != end(numbers); ++it) {
        //container.begin() or begin(container) both valid syntax
        //both return iterators
        cout << *it << " ";//isiliye pointer
    }

    cout << "\n";

    for (auto it = begin(arr); it != end(arr); ++it) {
        cout << *it << " ";
    }

    cout << "\n";


    auto thirdElm = next(next(begin(numbers))); //1 ke next ka next
    cout << "Third element of numbers is: " << *thirdElm << "\n";

    cout << "Third element of arr is: " << *(next(next(begin(arr))))<< "\n";

    cout << "There are " << distance(begin(numbers), end(numbers)) << " numbers\n";//distance(start, end) start se end tak ka distance
    cout << "and " << distance(begin(arr), end(arr)) << " array numbers\n";

    return 0;
}





