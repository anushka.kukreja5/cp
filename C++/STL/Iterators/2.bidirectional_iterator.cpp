

#include <iostream>
#include <iterator>
#include <vector>

using namespace std;

int main() {

    vector<int> numbers {1, 2, 3, 4, 5};

    //Note that numbers.end() points to 1 past the end
    for (auto it = numbers.begin(); it != numbers.end(); ++it) { //increment on iterator
        *it *= 10;
    }

    for (auto it = std::prev(numbers.end()); it >= numbers.begin(); --it) { //decrement on iterator
        *it -= 5;
    }

    for (int i : numbers) {
        cout << i << " ";
    }

    cout << "\n";

    return 0;
}
