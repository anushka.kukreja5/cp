
//Put Title as Iterators
#include <iostream>
#include <iterator>
#include <vector>

using namespace std;

int main() {

    vector<int> forwardVector = {1, 2, 3};

    vector<int>::iterator fit = forwardVector.begin();
    // You can access the same element multiple times in forward iterators
    // this is the difference between forward iterators and input iterators
    *fit = 20; //20, 2, 3
    *fit = 30; //30, 2, 3
    fit++;

    for (int i : forwardVector) {
        cout << i << " ";
    }

    cout << "\n";

    return 0;
}
