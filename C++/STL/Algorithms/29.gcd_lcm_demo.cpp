#include<iostream>
#include<numeric>
#include<algorithm>
#include<iterator>
#include<vector>

using namespace std;

int main() {
    int i = 5, j = 6; //35 51
    cout << gcd(i,j) << ", " << lcm(i, j) << "\n";
    i = 8;
    j = 12;
    cout << gcd(i,j) << ", " <<lcm(i,j) << "\n";
    vector<int> num1 {6, 3, 8, 12, 24, 13, 5, 35};
    vector<int> out;
    //Use it as binary predicate
    adjacent_difference(begin(num1), end(num1), back_inserter<vector<int>>(out), std::gcd<int, int>);
    for_each(begin(out), end(out), [](int i) {cout<<i<<" ";});
    cout<<"\n";
    return 0;
}
