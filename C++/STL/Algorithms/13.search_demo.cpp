#include<algorithm>
#include<iterator>
#include<iostream>
#include<vector>
#include<string>
#include<cctype>
#include<functional>

using namespace std;

bool equal_ignore_case(unsigned char rc, unsigned char sc) {
    return tolower(rc) == tolower(sc);
}

auto searchString(const string& ref, const string& s, function<bool(unsigned char, unsigned char)> pred = std::equal_to<unsigned char>()) {
    //Note: search returns the forward iterator
    return search(begin(ref), end(ref), begin(s), end(s), pred);
}

int main() {
    string dna = "GATATATACAGGTAGATACAGATATAGATATAGA";
    string text = "Can i match my joHn?";

    auto it = searchString(dna, "GATACA");
    if(it != end(dna)) {
        cout << "Found GATACA at position: " << distance(cbegin(dna), it) << "\n";
    }

    it = searchString(text, "john", equal_ignore_case);
    if(it != end(text)) {
        cout << "Found john at position: " << distance(cbegin(text), it) << "\n";
    }
    return 0;
}
