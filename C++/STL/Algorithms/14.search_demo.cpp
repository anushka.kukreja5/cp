#include<algorithm>
#include<iterator>
#include<iostream>
#include<string>
#include<functional>

using namespace std;

int main() {
    string in = "lorem ipsum";
    string needle = "pisci";

    auto it = search(in.begin(), in.end(),std::boyer_moore_horspool_searcher(needle.begin(), needle.end()));

    if(it != end(in)) {
        cout << "the String "<< needle << " found at offset " << distance(in.begin(), it)<<endl;
    } else {
        cout << "the String " <<needle << "was not found!" <<endl;
    }
    return 0;
}
