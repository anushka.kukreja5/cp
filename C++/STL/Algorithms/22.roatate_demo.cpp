#include<iostream>
#include<algorithm>
#include<iterator>
#include<vector>

using namespace std;

int main() {
    vector<int> i {0,1,2,3,4,5,6,7};
    vector<int> v_copy;

    cout << "Before Rotation: " << endl;
    for_each(begin(i), end(i), [](const auto& i) { cout << i << " ";});
    cout << "\n";

    rotate(begin(i), begin(i) + 2, end(i));

    cout << "After Rotation: " << endl;
    for_each(begin(i), end(i), [](const auto& i) { cout << i << " ";});
    cout << "\n";

    rotate_copy(rbegin(i), rbegin(i) + 5, rend(i), back_inserter(v_copy));

    cout << "After rotate copy vector i: " << endl;
    for_each(begin(i), end(i), [](const auto& i) { cout << i << " ";});
    cout << "\n";
    cout << "After rotate copy vector i: " << endl;
    for_each(begin(v_copy), end(v_copy), [](const auto& i) { cout << i << " ";});
    cout << "\n";

    return 0;
}
