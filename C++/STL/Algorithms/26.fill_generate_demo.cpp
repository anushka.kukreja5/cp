#include<iostream>
#include<iterator>
#include<algorithm>

using namespace std;

struct FibGenerator {
    FibGenerator() {
        n=1;
        n_1 = 1;
    }
    int operator()() {
        int out = n_1;
        int next = n+ n_1;
        n_1 = n;
        n = next;
        return out;
    }
private:
    int n;
    int n_1;
};
int main() {
    int values[10];
    fill(begin(values), end(values), 0);
    values[2] = 55;
    for_each(begin(values), end(values),  [](int v) {
             cout <<v <<" ";
    });
    cout << "\n";

    //creating object
    FibGenerator fib;
    generate(begin(values), end(values), fib); //Note here operator() is called when we are passing  obj in place of generator
    for_each(begin(values), end(values), [](int v) {
             cout << v<<" ";
        });
    cout << "\n";
    return 0;

}
