#include<iostream>
#include<numeric>
#include<algorithm>
#include<iterator>
#include<vector>

using namespace std;

int main() {
    vector<int> set1(20);
    vector<int> set2(20);

    iota(begin(set1), end(set1), 1);
    iota(begin(set2), end(set2), 1);

    for_each(begin(set1), end(set1), [](int i) { cout << i << " ";});
    cout << "\n";

    for_each(begin(set2), end(set2), [](int i) { cout << i << " ";});
    cout << "\n";

    int v = inner_product(begin(set1), end(set1), begin(set2), 0);
    cout << "inner product is: " << v << endl;

    v = inner_product(begin(set1), end(set1), begin(set2), 0, std::plus<int>(), std::plus<int>());
    cout << "sum of sums is: " << v << endl;

    return 0;
}
