#include<iostream>
#include<iterator>
#include<vector>
#include<algorithm>
#include<functional> //function

using namespace std;

template<typename T>
struct Sum {
    T sum; //a variable 'sum' of template T

    Sum() : sum{} {}
    //sum here is initialised to 0
    void operator() (T i) {
        sum+=i;
    }
};
template<typename T>
void print(T val) {
    cout << val << endl;
};
int main() {
    vector<int> numbers{2,4,6,7,10,12,14,16,18,20};

    std::function<void (int)> printFunction = &print<int>;
    //function returns void & accepts 1 integer as an argument

    for_each(begin(numbers), end(numbers), printFunction);
    //for_each(start, end, fn to be called)

    cout << endl;

    for_each(begin(numbers), end(numbers), [](int& i) { i+= 1;});
    Sum<int> sum = for_each(begin(numbers), end(numbers), Sum<int>());//har ek ke liye Sum create kiya

    cout << sum.sum <<"\n";
    return 0;
}
