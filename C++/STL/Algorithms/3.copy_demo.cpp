#include<iostream>
#include<algorithm>
#include<iterator>
#include<vector>

using namespace std;

int main() {
    vector<int> v{0,1,2,3,4,5,6,7,8,9,10};
    vector<int> v_copy(v.size());

    copy(begin(v), end(v), begin(v_copy));

    cout<<"Elements from v_copy after copy: "<<endl;
    for(auto itr = v_copy.begin(); itr!= v_copy.end(); ++itr) {
        cout << *itr << " ";
    }
    cout<<endl;

    v_copy.clear();

    copy_if(begin(v), end(v), back_inserter(v_copy), [](int i) { return i%3 ==0;});

    cout<< "Elements from v_copy after copy with predicate: "<<endl;
    for(auto itr = v_copy.begin(); itr!= v_copy.end(); ++itr) {
        cout << *itr << " ";
    }
    cout<<endl;

    auto it = begin(v);
    advance(it,3);

    copy_n(it,5, back_inserter(v_copy));

    cout << "Elements from v_copy after copy: "<<endl;
    for(auto itr = v_copy.begin(); itr!= v_copy.end(); ++itr) {
        cout << *itr << " ";
    }
    cout<<endl;
    return 0;
}


/*

Syntax of back_inserter:
template<class Container>
back_insert_iterator<Container> back_inserter (Container& x);

Description:
Constructs a back-insert iterator that inserts a new elements at the end of x.
A back-insert iterator is a special type of output iterator designed to allow algorithms that usually overwrite elements (such as copy) to instead insert new elements automatically at the end of the container.
It internally calls push_back() on the container. So we need not worry about the allocated size.
It can be used wherever Output is expected.

*/
