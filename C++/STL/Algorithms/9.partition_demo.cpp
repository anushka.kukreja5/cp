#include<algorithm>
#include<iostream>
#include<functional>
#include<iterator>
#include<cctype>
#include<typeinfo>

using namespace std;

bool predicate(unsigned char c) {
    return isupper(c);
}

int main() {
    string s = "This Is Some String Of text In Mixed Case";
    if(!is_partitioned(begin(s), end(s), predicate)) {
        partition(begin(s), end(s), predicate);
    }

    cout << "String now: ";
    cout << s << "\n";

    auto part = partition_point(begin(s), end(s), predicate); //returns an iterator
    cout << "True bucket: ";
    for(auto i = begin(s); i !=part; ++i) {
        cout << *i;
    }
    cout <<"\n";

    cout << "False bucket: ";
    for(auto i = part; i < s.end(); ++i) {
        cout << *i;
    }

    cout <<"\n";
    return 0;
}
