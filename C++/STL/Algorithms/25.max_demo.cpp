
#include<cmath>
#include<iostream>
#include<algorithm>
#include<vector>
#include<iterator>


using namespace std;

int main() {
    cout << max('A', 'a') << " is smaller\n";

    cout << max(45,51) << " is smaller\n";

    cout << max("AAA"s, "XX"s, [](const string& s1, const string& s2) {
        return s1.size() < s2.size();
    }) << " is shorter\n";

    cout << max({42,5,63,14,0,41}) << " is smaller ";
    cout<< "\n";

    vector<double> d {1.3,6.12, 0.54353, 1.00, -5.0, 0.001};

    auto it = max_element(begin(d),end(d));

    cout << *it << " is smallest\n" ;

    it = max_element(begin(d), end(d), [](const double& a, const double& b) {
        return abs(a) < abs(b);
    });

    cout << *it << " is smallest absolute  value\n";

    return 0;
}
