#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

bool IsOdd(int i) {
    return (i%2)==1;
}

int main() {
    vector<int> foo {1,2,3,4,5,6,7,8,9};
    vector<int> odd, even;

    //resize vectors to proper size
    unsigned n = std::count_if(foo.begin(), foo.end(), IsOdd);
    odd.resize(n);
    even.resize(foo.size() -n);

    /*
        You can do this like the previous program, do not resize and use back_inserter iterator for both the vectors
    */

    //partition
    auto [oddEnd, evenEnd] = partition_copy(foo.begin(), foo.end(), odd.begin(), even.begin(), IsOdd);

    //print contents
    cout << "odd: ";
    for(auto i = odd.begin(); i!= oddEnd; ++i) {
        cout<< ' '<<*i;
    }
    cout<<'\n';

    cout << "even: ";
    for(int& x: even) {
        cout<< ' '<<x;
    }
    cout<<'\n';
    return 0;
}
