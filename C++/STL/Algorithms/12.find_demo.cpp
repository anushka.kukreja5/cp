#include<iiostream>
#include<algorithm>
#include<iterator>
#include<string>
#include<vector>
#include<numeric>//iota

using namespace std;

int main() {
    string s = "This is some text, with some characters. Not every charcater is a letter, some, like this one '3' are numbers.";

    vector<int> num(100);

    std::iota(begin(num), end(num), 1);

    auto it = find(begin(s), end(s), ',');
    cout << "Found ',' at psoition: " << distance(begin(s), it) << "\n";

    auto number = find_if(begin(num), end(num), [](int i) {
        return (i% 3 == 0 && i% 5 == 0);
    });

    cout << "Found first number divisible by 3 & 5 at position: " << distance(begin(num), number << " (" <<number <<")\n";

    //the following code will print all the elements that are divisible by3 & 5;
    number = begin(num);
    while(number != end(num)) {
        number = find_if(number), end(num), [](int i) {
            return (i% 3 == 0 && i% 5 == 0);
        });
        if(number != end(num)) {
            cout << *number << " ";
            advance(number,1); //very imp do not forget
        }
    }
    return 0;
}
