#include<iostream>
#include<vector>
#include<algorithm>
#include<functional>

using namespace std;

int main() {

    vector<int> v{5,6,4,3,2,6,7,9,3};

    //The below will give median of the array
    nth_element(v.begin(), v.begin() + v.size()/2, v.end());
    cout << "The median is: " << v[v.size()/2] << '\n';

    for_each(begin(v), end(v), [] (auto& i) {cout << i << " ";});
    cout << endl;

    //The below can be used to get 2nd largest or first 2 larger numbers from the array
    nth_element(v.begin(), v.begin()+1, v.end(), std::greater<int>());
    cout<< "the second largest element is " << v[1] << '\n';

    for_each(begin(v), end(v), [] (auto& i) {cout << i << " ";});
    cout << endl;

    partial_sort(begin(v), begin(v) +v.size()/2, end(v));
    cout << "array after partial sort"<< endl;

    for_each(begin(v), end(v), [] (auto& i) {cout << i << " ";});
    cout << endl;

    return 0;
}
/*
nth_element is very similar to quickSort as quickSort sorts the complete array but nth_element only sorts the element/ index told to sort
*/
