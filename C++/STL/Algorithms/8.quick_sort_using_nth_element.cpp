#include<vector>
#include<iostream>
#include<algorithm>

using namespace std;

template<class RandomAccessIterator>
void quick_sort(RandomAccessIterator start, RandomAccessIterator end) {
    size_t dist = end - start + 1;

    //Stop condition for recursion.
    if(dist > 2) {
        //use nth element to do all the work for quick_sort
        nth_element(start, start + (dist / 2), end);

        //Recursive calls to each remaining unsorted portion
        quick_sort(start, start + (dist / 2));

        quick_sort(start + (dist / 2 ), end);

    }
    //special case for 2 elements
    if(2 == dist && *(end) < *start) {
//        std::swap(start, end);
            std::swap(*start,*(end-1));
            //end pointer hai isiilye uska address le raha tha swap kaarne ke liye
            //humko end jaha point kar raha hai waha ka addr dena hai

    }
}
int main() {
    vector<int> numbers{1, 10, 2, 9, 3, 8, 4, 7, 5, 6};

    //Print initial vector

    cout << "The unsorted vector is: ";
    for(auto i = begin(numbers); i< end(numbers); i++) {
        cout << *i << " ";
    }
    cout << endl;
    quick_sort(numbers.begin(), numbers.end());

    cout << "The sorted vector is: ";
    for(auto i = begin(numbers); i< end(numbers); i++) {
        cout << *i << " ";
    }

}
