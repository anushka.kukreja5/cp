#include <stdio.h>
#include <conio.h>
#define MAX 1000
int getMedian(int x[], int low, int middle,int high)
{

if((x[low] < x[middle] && x[middle] < x[high]) || (x[low] > x[middle] && x[middle] > x[high]))
    return middle;
if((x[middle] < x[low] && x[low] < x[high]) || (x[middle] > x[low] && x[low] > x[high]))
    return low;
return high;
}

void swap(int *pt1, int *pt2) {
    int temp;
    temp = *pt1;
    *pt1= *pt2;
    *pt2 = temp;
}
int partition(int x[], int lb, int ub) {
    int pivot;
    int down, up, medianIdx;
    down = lb;
    up = ub;
    medianIdx = getMedian(x,lb,(lb+ub)/2,ub);
    swap(&x[lb], &x[medianIdx]); //jo original quick sort haiuske algo me kuch changes nahi karna  padeisisiliye median and lb ko swap kar ke pivot ko lb se he initialize kar rahe
    pivot = x[lb];

    while(down < up) {
        while(x[down] <= pivot && down < ub )
            down ++;
        while(x[up] > pivot)
            up--;
        if(down < up)
            swap(&x[down], &x[up]);
    }
    swap(&x[lb], &x[up]);
    return up;
}
void quickSort(int x[], int lb, int ub) {
    int stack[MAX], top =-1;
    int partitionPoint;

    int low, high;

    stack[++top] = lb;
    stack[++top] = ub;

    while(top > 0) {
        high = stack[top--];
        low = stack[top--];

        partitionPoint = partition(x,low,high);

        if(partitionPoint - 1 > low) {
            stack[++top] = low;
            stack[++top] = partitonPoint- 1;
        }


        //checks if single element hai kya for right side aur upar me for left side

        if(partitionPoint + 1 < high) {
            stack[++top] = partitionPoint+ 1;
            stack[++top] = high;
        }
    }


}

int main() {
    int x[max];
    int length, i;

    printf("\nEnter the length of array: ");
    scanf("%d",&length);

    printf("\nEnetr %d elements one by one: ", length);
    for(i = 0;i < length; i++) {
        scanf("%d",x[i]);
    }

    quickSort(x,0,length);
    return 0;

}
//median hum isisliye nikaal rahe hai kyuki quick sort ka worst case is n^2 usme array is divided into single element and (n-1) elements toh har baar aise chalat hai isisliye pivot ko hum middle value se initialze karenge middle matlab jo bhi middle index pe baitha hoga par usme bhi aisa ho sakta hai ki array again aisa ho ki woh eleemnt lb pe jaa ke baithe hai and again array is divided into single and (n-1) elements toh aisa nahi ho isiliye hum median nikaalte hai 3 elements ka and pivot usko banate hai isse quick sort ka worst case complexity bhi n logn pe aa jaat ahai and also humne recusrive se hatay ahai aise he isiliye khud ka stack call maintain karna oad raha

}
