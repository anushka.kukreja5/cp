#include<iostream>
#include<vector>
#include<numeric>
#include<functional> //has multiplies
#include<iterator>

using namespace std;

int main() {
    vector<int> numbers {10,3,-5,12,5,35,11,-19};
    int sum = accumulate(begin(numbers), numbers.end(), 0);
    long product = accumulate(begin(numbers), end(numbers), 1, std::multiplies<long>());
    cout << "sum: " <<sum <<"\n";
    cout << "product: " <<product <<"\n";

}
