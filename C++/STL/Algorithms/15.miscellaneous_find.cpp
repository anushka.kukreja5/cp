#include<algorithm>
#include<iterator>
#include<iostream>
#include<vector>
#include<string>
#include<cctype>
#include<functional>

using namespace std;
int main() {
    vector<int> numbers {10,11,0,1,2,3,4,5,6,6,7,8,9,10,10,11};

    auto it = adjacent_find(begin(numbers), end(numbers));
    //numbers found in consecutive order will be counted.
    cout << "the number " << *it << " occurs at least twice in a row\n";

    vector<int> values {7,2,4};
    it = find_first_of(begin(numbers), end(numbers),begin(values), end(values));
    cout << "found " <<*it <<" first\n";

    vector<int> seq {10,11};
    it = find_end(begin(numbers), end(numbers), begin(seq), end(seq));
    cout << "last occurence of 10, 11 found at position: " << distance(begin(numbers), it) << "\n";

    string s = "Please, this is a simple string";
    string vow = "aeiou";
    auto it_s = find_first_of(begin(s), end(s), begin(vow), end(vow));
    cout<<"Found " <<*it_s<<" first!\n";

    return 0;
}
