#include<iostream>
#include<algorithm>
#include<vector>

using namespace std;
//there should be n! permutations where n is the no. of elements in the set.
int main() {
    //string vowels = "uoeia";
    string vowels = "aeiou";
    string test = "iuoea";
    string test2 = "iuoe";

    cout << std::boolalpha;
    cout << is_permutation(begin(test),end(test),begin(vowels)) << "\n";
    cout << is_permutation(begin(test2),end(test2),begin(vowels)) << "\n";

    vector<string> all_perm;

    all_perm.push_back(vowels);
    while(next_permutation(begin(vowels), end(vowels))) {
        cout << vowels << endl;
        all_perm.push_back(vowels);
    }
    cout << "There are total: " <<all_perm.size() << " permutations\n";
    return 0;
}
