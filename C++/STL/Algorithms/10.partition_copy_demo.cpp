#include<iostream>
#include<functional>
#include<algorithm>
#include<cctype>
#include<iterator>

using namespace std;

bool predicate(unsigned char c) {
    return isupper(c);
}

int main() {
    string s = "This Is Some String Of text In Mixed Case";
    string s_true;
    string s_false;

    partition_copy(begin(s), end(s), back_inserter(s_true), back_inserter(s_false), predicate);
    //              start    end        add if true             add if false        acc to this
    cout << s << "\n";
    cout << s_true << "\n";
    cout << s_false << "\n";
    return 0;
}
