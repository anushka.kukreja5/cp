#include<algorithm>
#include<iostream>
#include<vector>

using namespace std;

struct Student {
    const int id;
    const string first;
    const string last;
    Student(int id, string first, string last) : id(id), last(last), first(first) {}
};

int main() {
    vector<int> numbers {1,2,3,4,5,5,5,6,7,8,8};
    vector<Student> students;
    students.emplace_back(0, "james","slocum"); //emplaces_back is same as push_back
    students.push_back({1, "john","smith"});//here manually {} dena padega stating ki wo ek obj hai
    //baaki it identifies that it is an object of Student
    students.emplace_back(2, "Brian","richards");
    students.emplace_back(3, "tina","blake");

    cout << "there are " << count(begin(numbers), end(numbers), 5) << ", 5s in the vector\n";
    int sCount = count_if(begin(students), students.end(), [](const Student& s) {
        return (tolower(s.last.c_str()[0]) == 's');
    });
    cout <<sCount << " student(s) have a last name with 'S'\n";
    return 0;
}

/*
Key note on emplace_back:
Now, it's true that when implicit conversions are involved, emplace_back() can be somewhat faster than push_back().
For example, in the code that we began with, my_vec.push_back("foo") constructs a temporary string from the string literal and then moves that string into the container, whereas my_vec.emplace_back("foo") just constructs the string directly in the container, avoiding the extra move.
*/

