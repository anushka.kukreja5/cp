using namespace std;

#include <iostream>
#include <vector>

class SubsetSum {
public:
    bool canPartition(const vector<int> &num, int sum) {
        int n = num.size();
        vector<bool> dp(sum + 1);


        dp[0] = true;

        for(int s = 1; s <= sum; s++) {
            dp[s] = (num[0] == s ? true : false);
        }

        for(int i = 1; i < n; i++){
            for(int s = sum; s >= 0; s--) {
                if(!dp[s] && s >= num[i]) {
                    dp[s] = dp[s - num[i]];
                }
            }
        }

        return dp[sum];
    }
};

int main(int argc, char *argv[]) {
    SubsetSum ss;
    //vector<int> num = {1, 2, 3, 4};
    //cout << ps.canPartition(num) << endl;
    //vector<int> num = {1, 1, 3, 4, 7};
    //cout << ps.canPartition(num) << endl;
    vector<int> num = {1, 3, 4, 8};
    int sum = 7;
    cout << ss.canPartition(num, sum) << endl;
}
