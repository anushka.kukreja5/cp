using namespace std;

#include<iostream>
#include<vector>

class PartitionSet {
public:
    bool canPartition(const vector<int> &num) {
        int sum = 0;
        for(int i=0; i< num.size(); i++) {
            sum += num[i];
        }

        //if 'sum' is an odd number,we cant have two subsets with equal sum
        if(sum%2 != 0) {
            return false;
        }

        sum /=  2;
        vector<vector<bool>> dp(n,vector<bool>(sum + 1));

        for(int i = 0; i < n; i++) {
            dp[i][0] = true;
        }

        for(int s = 1; s <= sum; s++ ) {
            dp[0][s] = (num[0] == s ? true : false);
        }

        for(int i =1; i < n; i++) {
            for(int s = 1; s<= sum; s++) {
                if(dp[i-1][s]) {
                    dp[i][s] = dp[i-1][s];
                }else if( s >= num[i]) {
                    dp[i][s] = dp[i-1][s-num[i]];
                }
            }
        }
        return dp[n-1][sum];
    }
};
private:
    bool canPartitionRecursive(vector<vector<int>> &dp, const vector<int> &num, int sum, int currentIndex) {
        //base check
        if(sum == 0) {
            return true;
        }

        if(num.empty() || currentIndex >= num.size()) {
            return false;
        }

        //if we have not already processed a similar problem
        if(dp[currentIndex][sum] == -1){
            if(num[currentIndex] <= sum) {
                if(canPartitionRecursive(dp,num,sum-num[currentIndex],currentIndex + 1)) {
                    dp[currentIndex][sum] = 1;
                    return true;
                }
            }

            dp[currentIndex][sum] = canPartitionRecursive(dp,num,sum,currentIndex + 1) ? 1 : 0;
        }

        return dp[currentIndex][sum] == 1 ? true : false;
    }
};

int main(int argc,char *argv[]) {
    PartitionSet ps;
    vector<int> num=  {1,2,3,4};
    cout << ps.canPartition(num) << endl;
    num = vector<int>{1,1,3,4,7};
    cout << ps.canPartition(num) <<endl;
    num = vector<int>{2,3,4,6};
    cout << ps.canPartition(num) <<endl;

}

//complexity : O(s)

