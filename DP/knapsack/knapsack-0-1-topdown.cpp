class Knapsack {
public:
    int solveKnapsack(vector<int> &profits,vector<int> &weights,int capacity) {
    vector<vector<int>> dp(profits.size(),vector<int>(capacity+1,-1));
    return solveKnapsackRecursive(dp,profits,weights,capacity,0);
    }

private:
    //idhar dp ka pass by reference zaruri hai pass by value kiya toh koi matlab he nahi hua phir yeh function ka
    int solveKnapsackRecursive(vector<vector<int>>&dp,vector<int> &profits,vector<int> &weights,int capacity,int currentIndex) {
        //Base condition
        if(capacity <=0 || currentIndex >= profits.size()) {
            return 0;
        }
        if(dp[currentIndex][capacity] != -1) {
            return dp[currentIndex][capacity];
        }

        int profit1 = 0;
        if(capacity >= weights[currentIndex]) {
            //With currentIndex Item
            profit1 = profits[currentIndex]  + solveKnapsackRecursive(dp,profits,weights,capacity-weights[             currentIndex],currentIndex+1);
        }
        //Without currentIndex item
        int profit2 =  solveKnapsackRecursive(dp,profits,weights,capacity,currentIndex+1);

        dp[currentIndex][capacity] = max(profit1,profit2);4
        return dp[currentIndex][capacity];
    }
}


