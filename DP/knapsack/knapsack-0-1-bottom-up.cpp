#include<vector>
#include<algorithm>
using namespace std;

class knapsack {
public:
    int solveKnapsack(vector<int> &profits, vector<int> &weights, int capacity) {
        //Validation check
        if(profits.size() == 0 || capacity < 0) {
            return 0;
        }
        vector<vector<int>> dp(2, vector<int>(capacity + 1, 0));


        int n = profits.size();


        //Fill the 1st column with 0 as we have 0 capacity, meaning no item can be selected, yielding 0 profit
        for(int i=0;i<n; i++) {
            dp[i][0] = 0;
        }

        for(int c = weights[0]; c<=capacity; c++) {
            dp[0][c] = weights[0] >= c ? profits[0] : 0;
        }

        for(int i=1; i<n; i++) {
            for(int c=1; c<=capacity; ++c) {
                int profit1 = 0, profit2 = 0;
                profit1 = dp[(i-1)%2][c]; // without current item
                if( c >= weights[i]) {
                    profit1 = profits[i] + dp[(i-1)%2][c-weights[i]];
                }

                profit2 = dp[(i-1)%2][c];

                dp[i%2][c] = max(profit1, profit2);
            }

        }

        return dp[(n-1)%2][capacity];
    }
};

