class Knapsack {
public:
    int solveKnapsack(vector<int> &profits,vector<int> &weights,int capacity) {
    return solveKnapsackRecursive(profits,weights,capacity,0);
    }

private:
    int solveKnapsackRecursive(vector<int> &profits,vector<int> &weights,int capacity,int currentIndex) {
        //Base condition
        if(capacity <=0 || currentIndex >= profits.size()) {
            return 0;
        }

        int profit1 = 0;
        if(capacity >= weights[currentIndex]) {
            //With currentIndex Item
            profit1 = profits[currentIndex]  + solveKnapsackRecursive(profits,weights,capacity-weights[             currentIndex],currentIndex+1);
        }
        //Without currentIndex item
        int profit2 =  solveKnapsackRecursive(profits,weights,capacity,currentIndex+1);

        return max(profit1,profit2);
    }
}


