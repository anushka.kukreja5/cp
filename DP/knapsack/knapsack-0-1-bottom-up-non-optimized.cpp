#include<vector>
#include<algorithm>
using namespace std;

class Knapsack {
public:
    int solveKnapsack(vector<int> &profits, vector<int> &weights, int capacity) {
        //Validation Check
        if(profits.size() == 0|| capacity <=0) {
            return 0;
        }
        int n = profits.size();

        vector<vector<int>> dp(n,vector<int>(capacity+1,0));

        //Fill the 1st column with 0 as we have 0 capacity, meaning no item can be selected, yielding 0 profit
        for(int i=0;i<n;i++) {
            dp[i][0] = 0;
        }

        for(int c=weights[0];c<=capacity;c++) {
            dp[0][c] = profits[0];
        }
        for(int i=1;i<n;i++) {
            for(int c=1;c<=capacity;++c) {
                int profit1 = 0, profit2 = 0;

                if(weights[i] <= c) {
                    profit1 = profits[i] + dp[i-1][c-weights[i]];
                }
                profit2 =dp[i-1][c];

                dp[i][c] = max(profit1,profit2);
            }
        }

        return dp[n][capacity];
    }
};
