#include<iostream>
#include<vector>
#include<limits>

using namespace std;

class CoinChange {
public:
    int countChange(const vector<int> &denominations, int total) {
        int n = denominations.size();
        vector<vector<int>> dp(n,vector<int>(total + 1));

        for(int i=0; i < n; i++) {
            for(int j = 0; j <= total; j++) {
                dp[i][j] = numeric_limits<int>::max();
            }
        }

        //populate 1st column
        for(int i=0;i<n;i++) {
            dp[i][0] = 0;
        }


        for(int i=0; i <n; i++) {
            for(int t=1; t<= total; t++) {
                if( i> 0) {
                    dp[i][t] = dp[i-1][t];
                }
                if( t >= denominations[i]) {
                    if(dp[i][t-denominations[i]]!= numeric_limits<int>::max()) {
                        dp[i][t] = min(dp[i][t],dp[i][t-denominations[i]] + 1);
                    }
                }
            }
        }

        return dp[n-1][total] == numeric_limits<int>::max() ? -1 : dp[n-1][total];

    }
};
