#include<iostream>
#include<vector>

using namespace std;

class Fibonacci {
public:
    int calculateFibonacci(int n) {
        if(n<2) {
            return n;
        }

        vector<int> dp(n+1);
        dp[0] = 0;
        dp[1] = 1;

        for(int i=2; i<=n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }

        return dp[n];

    }
};

0 1 2 3 4 5 6 7
0 1 1 2
int main() {
    Fibonacci *fib = new Fibonacci();

    cout <<"5th Fibonacci is ---> " << fib->calculateFibonacci(5)<<endl;
    cout <<"6th Fibonacci is ---> " << fib->calculateFibonacci(6)<<endl;
    cout <<"57th Fibonacci is ---> " << fib->calculateFibonacci(7)<<endl;

    delete fib;
}
