using namespace std;

#include<iostream>
#include<vector>

class Fibonacci {
public:
    int calculateFibonacci(int n) {
        vector<int> dp(n+1,0);
        return calculateFibonacciRecursive(dp,n);
    }


private:
    int calculateFibonacciRecursive(vector<int> &dp, int n) {
        if(n < 2) {
            return n;
        }

        if(dp[n] == 0) {
                        //6                                 //5
            dp[n] = calculateFibonacciRecursive(dp,n-1) + calculateFibonacciRecursive(dp,n-2);
        }

        return dp[n];
    }
};

int main()
{
    Fibonacci *fib = new Fibonacci();

    cout <<"5th Fibonacci is ---> " << fib->calculateFibonacci(5)<<endl;
    cout <<"6th Fibonacci is ---> " << fib->calculateFibonacci(6)<<endl;
    cout <<"57th Fibonacci is ---> " << fib->calculateFibonacci(7)<<endl;

    delete fib;

}
