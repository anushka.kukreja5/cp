#include <iostream>
#include <vector>
using namespace std;
class Staircase
{
public:
    int countWays(int n)
    {
        vector<int> dp(n + 1, 0);
        return this->countWaysRecursive(dp, n);
    }
    int countWaysRecursive(vector<int> &dp, int n)
    {
        if (n == 0)
        {
            return 1; // base case,we dont need to take any step,so there is only one way
        }
        if (n == 1)
        {
            return 1; // we can take one step to reach to end,and that is the only way
        }
        if (n == 2)
        {
            return 2; // we can take one step twice or jump two steps to reach to the top
        }
        if (dp[n] == 0)
        {
            int take1Step = countWays(n - 1);
            int take2Step = countWays(n - 2);
            int take3Step = countWays(n - 3);

            dp[n] ==  take1Step + take2Step + take3Step;
        }
        return dp[n];
    }
};