using namespace std;
#include <iostream>
#include<vector>

class SubsetSum {
public:
    int countSubsets(const vector<int> &num,int sum) {
        int n = num.size();
        vector<vector<int>> dp(n,vector<int>(sum+1,0));

        for(int i = 0;i < n;i++) {
            dp[i][0] = 1;
        }
        for(int s =1 ;s <= sum; s++ ) {
            dp[0][s] = (num[0] == s ? 1 : 0);
        }

        for(int i = 1; i < num.size();i ++) {
            for(int s = 1; s <= sum; s++) {
                dp[i][s] = dp[i-1][s];
                if(s >= num[i]) {
                    dp[i][s] += dp[i-1][s-num[i]];
                }
            }
        }
        return dp[num.size()-1][sum];
    }
};
