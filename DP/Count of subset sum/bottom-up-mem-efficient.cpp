using namespace std;
#include <iostream>
#include<vector>

class SubsetSum {
public:
    int countSubsets(const vector<int> &num,int sum) {
        int n = num.size();
        vector<int> dp(sum+1);


            dp[0] = 1;

        for(int s =1 ;s <= sum; s++ ) {
            dp[s] = (num[0] == s ? 1 : 0);
        }

        for(int i = 1; i < num.size();i ++) {
            for(int s = sum; s >= 0; s--) {

                if(s >= num[i]) {
                    dp[s] += dp[s-num[i]];
                }
            }
        }
        return dp[sum];
    }
};
int main(int argc,char *argv[]) {
    SubsetSum ss;
    vector<int> num=  {1,2,3,4};
    cout << ss.countSubsets(num,7) << endl;
    num = vector<int>{1,1,3,4,7};
    cout << ss.countSubsets(num,9) << endl;
    num = vector<int>{2,3,4,6};
    cout << ss.countSubsets(num,10) << endl;

}
