using namespace std;

#include <iostream>
#include <vector>

class SubsetSum {
public:
    int countSubsets(const vector<int> &num, int sum) {
        return this->countSubsetsRecursive(num,sum,0);
    }

private:
    int countSubsetsRecursive(const vector<int> &num,int sum,int currentIndex) {
        //base check
        if(sum == 0) {
            return 1;
        }

        if(num.empty() || currentIndex >= num.size()) {
            return 0;
        }

        int sum1 = 0;
        if(num[currentIndex] <= sum) {
            sum1 = countSubsetsRecursive(num,sum-num[currentIndex],currentIndex+1);
        }

        int sum2 = countSubsetsRecursive(num,sum,currentIndex +1);

        return sum1+sum2;
    }
};
int main(int argc,char *argv[]) {
    SubsetSum ss;
    vector<int> num=  {1,2,3,4};
    cout << ss.countSubsets(num,7) << endl;
    num = vector<int>{1,1,3,4,7};
    cout << ss.countSubsets(num,9) << endl;
    num = vector<int>{2,3,4,6};
    cout << ss.countSubsets(num,10) << endl;

}
