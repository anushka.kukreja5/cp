using namespace std;

#include <iostream>
#include <vector>

class SubsetSum {
public:
    int countSubsets(const vector<int> &num,int sum) {
        vector<vector<int>> dp(num.size(),vector<int>(sum+1,-1));
        return this->countSubsetRecursive(dp,num,sum,0);
    }

private:
    int countSubsetRecursive(vector<vector<int>> &dp, const vector<int> &num,int sum,int currentIndex) {
    //base checks
    if(sum == 0) {
        return 1;
    }

    if(num.empty() || currentIndex >= num.size()) {
        return 0;
    }

    if(dp[currentIndex][sum] == -1) {
        int sum1 = 0;
        if(num[currentIndex] <= sum) {
            sum1 = countSubsetRecursive(dp,num,sum-num[currentIndex],currentIndex+1);
        }

        int sum2 = countSubsetRecursive(dp,num,sum,currentIndex+1);

        dp[currentIndex][sum] = sum1 + sum2;
    }

    return dp[currentIndex][sum];
    }
};
int main(int argc,char *argv[]) {
    SubsetSum ss;
    vector<int> num=  {1,2,3,4};
    cout << ss.countSubsets(num,7) << endl;
    num = vector<int>{1,1,3,4,7};
    cout << ss.countSubsets(num,9) << endl;
    num = vector<int>{2,3,4,6};
    cout << ss.countSubsets(num,10) << endl;

}

