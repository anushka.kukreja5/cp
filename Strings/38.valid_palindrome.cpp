#include<iostream>
#include<vector>

using namespace std;

bool helper(string str, int i) {
    int j = str.length()-1-i;
    if(i >= j) {
        return true;
    }else {
        if(str[i] == str[j]) {
            helper(str,i+1);
        }else {
            return false;
        }
    }
    //return i>=j ? true : str[i] == str[j] && helper(str,i+1);
}


//Time:O(n)
//Space: O(n)

bool isPalindrome(string str) {
    //Sanitizing the input
    vector<char> cleanChars;
    for(int i=0;i<str.length();++i) {
        if(isalnum(str[i])) {
            cleanChars.push_back(tolower(str[i]));
        }
    }
    string cleanStr = string(begin(cleanChars),end(cleanChars));
    return helper(cleanStr,0);
 }

 //Time: O(n)
 //Space :O(1)
/*
 bool isPalindrome(string str) {
    int left = 0,right  = str.length() - 1;

    while(left  <right) {
        while(left < right && !isalnum(str[left])) {
            ++left;
        }
        while(left < right && !isalnum(str[right])) {
            --right;
        }
        if(tolower(str[left]) != tolower(str[right])) {
            return false;
        }
        ++left;
        --right;
    }
    return true;
}*/
int main() {
    string str = "Racecar";
    bool result = isPalindrome(str);
    cout<<boolalpha;
    cout<<result<<endl;

    return 0;
}
