#include<iostream>
#include<unordered_set>
#include<numeric>

using namespace std;

string removeVowels(string S) {
    return accumulate(S.begin(),S.end(),string{},[](const auto &a,const auto &b){
                      static const unordered_set<char> lookup = {'a','e','i','o','u'};
                      if(!lookup.count(b)) {
                        return a + b;
                      }
                      return a;
      });
}
int main() {
    string s ="atastructureisfunny";
    string res = removeVowels(s);
    cout<<res<<endl;
    return 0;
}
