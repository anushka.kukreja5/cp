#include<iostream>
#include<string>

using namespace std;

string reduceString(string s) {
    int j=0;
    for(int i=1;i<s.size();i++) {
        while(j>=0 && s[i] == s[j]) {
            j--;
            i++;
        }
        s[++j] = s[i];
    }
    s.erase(j+1);
    return s;
}
int main() {
    string s = "abcccbcba";
    string res = reduceString(s);
    cout<<res<<endl;
    return 0;
}
