#include<iostream>
#include<vector>
#include<unordered_map>
#include<stack>

using namespace std;

bool isBalanced(string str) {
        unordered_map<char,char> hashmap;
        hashmap[')'] = '(';
        hashmap['}'] = '{';
        hashmap[']']='[';


        stack<char> stack;


        for(int i=0;i<str.length();++i) {
            if(str[i] == '(' || str[i] == '{' || str[i] == '[') {
                stack.push(str[i]);
            }else {
                if(hashmap[str[i]] == stack.top()) {
                    stack.pop();
                }
                else {
                    return false;
                }
            }

        }

        return stack.empty();

}
int main() {
    cout<<boolalpha;
    bool result =  isBalanced("[{}{()}]");

    cout<<result<<endl;
    return 0;
}
