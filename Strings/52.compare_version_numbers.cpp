#include<iostream>

using namespace std;

int compareVersion(string version1,string version2) {
    const int len1 = version1.size();
    const int len2 = version2.size();

    for(int i=0,j=0;i<len1 || j<len2;i++,j++) {
        int v1 = 0;
        int v2 = 0;
        while(i<len1 && version1[i] != ".") {
            v1 = v1 *10 + (version1[i++] - '0');
        }
        while(j<len2 && version2[j] != ".") {
            v2 = v2 * 10 + (version2[j++] - '0');
        }
        if(v1 != v2) {
            return v1 > v2 ? 1 : -1;
        }
    }
    return 0;
}
int main() {
    string version1 = "7.5.02.4";
    string version2 = "7.05.2.004";
    int result = compareVersion(version1,version2);
    cout<<result<<endl;
    return 0;
}
