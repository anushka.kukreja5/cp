#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
#include<numeric>

using namespace std;

string encode(vector<string> &strs) {
    string s;
    for(size_t i=0;i<strs.size();++i) {
        size_t len = strs[i].length();
        for(int j=sizeof(size_t)-1; j>=0;--j) {
            s.push_back((len >> (8*j)) & 0xFF);
        }
        s.append(strs[i]);
    }
    return s;
}

vector<string> decode(string str) {
    vector<string> result;

    size_t pos = 0;
    while(pos < str.length()) {
        size_t len =0;
        for(size_t i=0; i<sizeof(size_t);++i) {
            len = (len << 8) | str[pos++];
        }
        result.push_back(str.substr(pos,len));
        pos += len;
    }
    return result;
}
int main() {
    vector<string> strs ={"bdeuhfiomxowxpkdæē","Study","Link","is","the","best"};
    string s = encode(strs);
    cout<<s<<endl;
    vector<string> ans = decode(s);

    for(string s: ans) {
        cout<<s<<" ";
    }
    return 0;

}
