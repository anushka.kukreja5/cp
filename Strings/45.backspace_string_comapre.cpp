#include<iostream>
#include<string>

using namespace std;

char findNextChar(const string& s,int *i) {
    int count = 0;
    while(*i >=0) {
        if(s[*i] == '#') {
            count++;
        } else if(count> 0) {
            count --;
        } else {
            return s[*i];
        }
        ((*i)--); //(--(*i)) //*i -=1;
    }
    return '\0';
}

bool backspaceComapre(string S, string T) {
    for(int i =S.length()-1, j = T.length() -1; i >=0 && j >=0; --i,--j) {
        if(findNextChar(S,&i) != findNextChar(T, &j)) {
            return false;
        }
    }
    return true;
}
int main() {
    string S = "ab#c";
    string T = "ad#c";

    bool res = backspaceComapre(S,T);
    cout<<boolalpha;
    cout<<res<<endl;
    return 0;
}
