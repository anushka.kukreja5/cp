#include<iostream>
#include<string>
#include<algorithm>

using namespace std;

string minRemovetoMakeValid(string s) {
    int count =0;
    for(auto i=0; i <s.size();++i) {
        if(s[i] == '(') {
            ++count;
       }

       if(s[i] ==')') {
            if(count) {
                -- count;
            } else {
                s[i] = '$';
            }
       }
    }
    if(count) {
        for(auto i=s.size()-1; i>=0;--i) {
            if(s[i] == '(') {
                s[i] = '$';
                count --;
                if(count == 0) {
                    break;
                }
           }
        }
    }
    s.erase(remove(begin(s),end(s),'$'),end(s));
    return s;
}
int main() {
    string s = "dab((abaacca)ddd()";
    string res = minRemovetoMakeValid(s);
    cout<<res<<endl;
    return 0;
}
