#include<iostream>
#include<vector>
#include<unordered_map>

using namespace std;

//Time: O(n)
//Space: O(1)

int lengthOfLongestSubstring(string s) {
    unordered_map<char,int> lookup;
    int currentMaxLength = 0;
    for(int left = 0, right = 0;right <s.length();++right) {
        if(lookup.count(s[right])) {
            int lastOccurenceIndex = lookup[s[right]];
            left = max(left, lastOccurenceIndex+1);
        }
        lookup[s[right]] = right;
        currentMaxLength = max(currentMaxLength, right - left+1);
    }

    return currentMaxLength;
}

int main() {
    string s = "competitiveprogramming";
    int res = lengthOfLongestSubstring(s);
    cout<<"Length of longest substring without repeating characters: "<<res<<endl;
    return 0;
}
