#include<iostream>
#include<string>

using namespace std;

string caesarCipher(string s, int k) {
    k =k%26;

    int len = s.length();
    for(int i=0; i<len; ++i) {
        if(s[i] >= 'a' && s[i] <= 'z') {
            s[i] = ((s[i] - 'a' + k)%26) + 'a';
        } else if(s[i] >= 'A' && s[i] <= 'Z') {
            s[i] = ((s[i] - 'A' + k)%26) + 'A';
        }
    }
    return s;
 }

 int main()
 {

     string s = "There's-a-starman-waiting-in-the-sky";

 }
