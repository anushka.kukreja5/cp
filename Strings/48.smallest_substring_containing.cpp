#include<iostream>
#include<climits>
#include<unordered_map>
#include<vector>
#include<string>

using namespace std;

void increaseCharCount(char c, unordered_map<char,int> &charCounts) {
    if(charCounts.find(c) == charCounts.end())
    {
        charCounts[c] = 1;
    }else {
        charCounts[c]++;
    }
}
unordered_map<char,int> getCharCounts(string s) {
    unordered_map<char,int> charCounts;
    for(auto c: s) {
        increaseCharCount(c,charCounts);
    }
    return charCounts;
}
vector<int> getSmallerBounds(int idx1,int idx2,int idx3,int idx4) {
    return idx2-idx1 < idx4 - idx3 ? vector<int>{idx1,idx2} : vector<int>{idx3,idx4};
}
vector<int> getSubstringBounds(string str,unordered_map<char,int> targetCharCounts) {
    vector<int> substringBounds = {0,INT_MAX};
    unordered_map<char,int> substringCharCounts;

    int numOfUniqueChars = targetCharCounts.size();

    int numOfUniqueCharsDone = 0;

    int leftIdx = 0;
    int rightIdx = 0;

    //Move the rightIdx to the right in the string until you have counted all the target characters required number of times
    while(rightIdx  <str.length()) {
        char rightChar = str[rightIdx];

        if(targetCharCounts.find(rightChar) == targetCharCounts.end()) {
            //rightChar not found, so just move the rightIdx and check for next chracter
            rightIdx++;
            continue;
        }
        increaseCharCount(rightChar,substringCharCounts);
        if(substringCharCounts[rightChar] == targetCharCounts[rightChar]) {
            numOfUniqueCharsDone++;
        }
        //Move the leftIdx to right until you do not have any remaining unique charcter tp search.
        while(numOfUniqueCharsDone == numOfUniqueChars && leftIdx <= rightIdx) {
            substringBounds = getSmallerBounds(leftIdx,rightIdx,substringBounds[0],substringBounds[1]);

            char leftChar = str[leftIdx];

            if(targetCharCounts.find(leftChar) == targetCharCounts.end()) {
                leftIdx++;
                continue;
            }
            if(substringCharCounts[leftChar] == targetCharCounts[leftChar]) {
                numOfUniqueCharsDone--;
            }

            substringCharCounts[leftChar]--;

            leftIdx++;
        }
        rightIdx++;
    }
    return substringBounds;
}
string smallestSubstringContaining(string bigString, string smallString) {
    unordered_map<char,int> targetCharCounts = getCharCounts(smallString);

    vector<int> substringBounds = getSubstringBounds(bigString,targetCharCounts);
    int startIdx = substringBounds[0];
    int endIdx = substringBounds[1];

    if(endIdx == INT_MAX) {
        return "";
    }
    return bigString.substr(startIdx,endIdx - startIdx+1);
}
int main() {
    string smallString = "ABC" ;
    string bigString = "ABFEBCATT";

    string result = smallestSubstringContaining(bigString,smallString);

    cout<<result<<endl;

}
