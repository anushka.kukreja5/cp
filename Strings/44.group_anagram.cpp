#include<iostream>
#include<string>
#include<vector>
#include<unordered_map>
#include<algorithm>

using namespace std;

//Time: O(W n log n): Here W is no. of words in str, and n is length of the longest word
//Space: O(Wn)
vector<vector<string>> groupAnagrams(vector<string>& strs) {
    unordered_map<string,vector<string>> groups;

    for(const auto& str: strs) {
        string tmp{str};
        sort(begin(tmp),end(tmp));
        groups[tmp].push_back(str);
    }

    vector<vector<string>> anagrams;
    for(const auto& kvp: groups) {
        anagrams.push_back(kvp.second);
    }
    return anagrams;
}

int main() {
    vector<string> strs = {"eat","tea","tan","ate","nat","bat"};
    vector<vector<string>> res = groupAnagrams(strs);

    for(vector items: res) {
        for(string s: items) {
            cout<<endl<<" ";
        }
        cout<<endl;
    }
    return 0;
}
