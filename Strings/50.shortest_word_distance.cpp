#include<iostream>
#include<vector>


using namespace std;

int shortestDistance(vector<string>& words,string word1, string word2) {
    int word1Idx, word2Idx,minDistance;
    word1Idx = word2Idx = -1;
    minDistance = words.size();

    for(int i=0;i<words.size();i++) {
        if(words[i] == word1) {
            word1Idx = i;
        }else if(words[i] == word2) {
            word2Idx = i;
        }

        if(word1Idx != -1 && word2Idx != -1) {
            minDistance = min(minDistance,abs(word1Idx - word2Idx));
        }
    } return minDistance;
}
int main() {
    vector<string> words {"practice","makes","a","man","perfect","makes"};
    int result = shortestDistance(words,"makes","perfect");
    cout<<result<<endl;
    return 0;
}

