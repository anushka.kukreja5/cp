#include<iostream>
#include<vector>

using namespace std;
/*
//Horizontal Scanning Approach 1
string longestCommonPrefix(vector<string>& strs) {
    //"STUDENT
    int minLength = INT_MAX;

    for(string s: strs) {
        int currentLength = s.size();
        minLength = min(minLength,currentLength);
    }
    //min length = 5;
    string possiblePrefix  =strs[0].substr(0,minLength);
    int j;
    for(int i=1;i<strs.size();i++) {
        string currentString = strs[i];
        for(j=0;j<possiblePrefix.siz();j++) {
            if(possiblePrefix[j] != currentString[j]) {
                break;
            }
        }
        if(j<possiblePrefix.size()) {
            possiblePrefix = possiblePrefix.substr(0,j); //(startIndex,endIndex)
        }
    }
    return possiblePrefix;
}
//Horizontal Scanning Approach 2
string longestCommonPrefix(vector<string>& strs) {
    if(strs.empty()) {
        return "";
    }
    string prefix = strs[0];
    for(int i=1;i<strs.size(); ++i) {
        while(strs[i].find(prefix) != 0) {
            prefix  = prefix.substr(0,prefix.length()-1 );
            if(prefix.empty()) return "";
        }
    }
}
*/
//Vertical Scaaning Approach 1
/*
string longestCommonPrefix(vector<string>& strs) {
    int minLength = INT_MAX;

    for(string s: strs) {
        int currentLength = s.size();
        minLength = min(minLength,currentLength);
    }
    //minLength = 5;
    string prefix = "";
    int j;
    for(int i=0;i<minLength;i++) {
        char currentCharacter = strs[0][i];
        for(j=1;j<strs.size();j++) {
            if(strs[j][i] != currentCharacter) {
                break;
            }
        }
        if(j<strs.size()) {
            return prefix;
        }
        prefix.push_back(currentCharacter);
    }
}
*/
//Vertical Scanning Approach 2
string longestCommonPrefix(vector<string>& strs) {
    if(strs.empty()) {
        return "";
    }
    for(int i = 0; i<strs[0].length();++i) {
        for(const auto& str: strs) {
            if(i >= str.length() || str[i] != strs[0][i]) {
                return strs[0].substr(0,i);
            }
        }
    }
    return strs[0];
}
int main() {
    vector<string> strs = {"flowers","flow","flight"};
    string result  = longestCommonPrefix(strs);
    cout<<result<<endl;
    return 0;
}
