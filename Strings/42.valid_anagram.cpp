#include<iostream>
#include<string>
#include<unordered_map>

#include<memory.h> //used for memeset function in approach 2

using namespace std;

//Method 1:Sort n compare_exchange_strong
//Time: O(n log n)
//Space: O(1)

//Method 2:
//Time: O(n)
//Space: O(1)

bool isAnagram(string s , string t) {
    if(s.length() != t.length())
        return false;

    unordered_map<char,int> counter;
    for(const char& c: s) {
        ++counter[c];
    }

    for(const char& c: t) {
        if(!counter.count(c))
            return false;
        --counter[c];
        if(counter[c] < 0)
        {
            return false;
        }
    }
    return true;

}
/*

//Method 3:
//Time : O(n)
//Space: O(1)

bool isAnagram(string s, string t) {
    if(s.length() != t.length()) {
        return false;
    }

    int hashmap[26];
    memset(hashmap,0,sizeof(hashmap));

    for(const char& c: s) {
        hashmap[c-'a']++;

    }
    for(const char& c: t) {
        hashmap[c-'a']--;
    }
    for(auto it: hashmap) {
        if(it != 0){
            return false;
        }
    }
    return true;
}
*/

int main() {
    string s = "anagram";
    string t = "nagrama";

    bool result = isAnagram(s,t);
    cout<<boolalpha;
    cout<<result<<endl;

    return 0;
}
