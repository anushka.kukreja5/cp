#include<iostream>
#include<vector>

using namespace std;

//Time: O(n)
//Space :O(1)

int maxArea(vector<int>& height) {
    int left = 0;
    int right = height.size()-1;
    int maxArea = 0;

    while(left <right) {
        int currentArea = (right - left)* min(height[left],height[right]);
        maxArea= max(maxArea, currentArea);
        if(height[left] > height[right]) {
            --right;
        } else {
            ++left;
        }
    }
    return maxArea;
}
int main() {
    vector<int> nums {1,8,6,2,5,4,8,3,7};
    int res = maxArea(nums);

    cout<< res;
    return 0;
}
