//relative order maintain karna hai
//time: O(n)
#include <iostream>
using namespace std;

void moveToEnd(int arr[], int n, int element) {
    int idx = 0;
    int i;
    for(i=0; i<n; ++i) {
        if(arr[i] !=element) {
            arr[idx++] = arr[i];
        }
    }

    while(idx < n) {
        arr[idx++] = element;
    }
}

int main() {
    int arr[] = {2,8,4,5,9,5,5,2,7,5};
    int n = sizeof(arr) / sizeof(arr[0]);
    moveToEnd(arr,n,5);
    cout << "Array after pushing all " <<5<<" to end of array: \n";
    for(int i=0; i< n; i++) {
        cout << arr[i] << " ";
    }
    return 0;
}
