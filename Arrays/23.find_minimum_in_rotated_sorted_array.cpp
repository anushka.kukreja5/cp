 #include<iostream>
#include<vector>
using namespace std;

int findMin(vector<int>& nums) {
    int low=0;
    int high = nums.size()-1;

    if(nums.size()==1 || nums[0] < nums[high]) {
        return nums[0];
    }

    while(low<=high) {
        int mid = low+  (high - low)/2; //this is for range assume int ka max range 100 hai and high is at 99 calclualte with normal formula then with this


        //checking whether mid is inflection point or not
        if(nums[mid] > nums[mid+1]) {
            return nums[mid+1];
        }
        if(nums[mid] < nums[mid-1]) {
            return nums[mid];
        }

        //end of check so now update low or high accordingly
        if(nums[mid] > nums[0]) {
            low = mid+1;
        }else {
            high= mid -1;
        }
    }
    return -1;
}
int main() {
    vector<int> nums {4,5,6,7,0,1,2};
    int res = findMin(nums);
    cout << res <<endl;
    return 0;
}
