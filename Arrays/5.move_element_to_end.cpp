//time: O(n);
//space: O(1)
#include <iostream>
using namespace std;

void moveToEnd(int arr[], int n, int element) {
    int i = 0;
    int j = n-1;

    while(i<j) {
        while(i<j && arr[j]== element) {
            j--;
        }//j is at the index where value is not qual to eleemnt isiliye yehidhar he aayega upar nahi
        if(arr[i] == element) {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
        i++;
    }
}
int main() {
    int arr[] = {2,1,2,2,2,3,4,2};
    int n = sizeof(arr) / sizeof(arr[0]);
    moveToEnd(arr,n,2);
    cout << "Array After pushing all " <<2<<"to end of array :\n";
    for(int i = 0; i < n; i++)
        cout << arr[i] << " ";
    return 0;
}
