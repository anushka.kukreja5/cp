#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
int maxProfit(std::vector<int>& prices) {
    //7,100,150,1,10,200
    int n= prices.size();
    int minPrice = prices[0]; // 7 is the smallest
    int maxProfit = 0;

    for(int i =1; i<n; i++) {
        minPrice = min(minPrice,prices[i]);
        maxProfit = max(maxProfit,prices[i] - minPrice);
    }
    return maxProfit;
}
int main() {
    std::vector<int> prices = {7,1,5,3,6,4};
    int result = maxProfit(prices);
    std::cout<<"MAximum Profit = " <<result;
    return 0;
}
