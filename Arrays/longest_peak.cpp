//find all peaks: O(n)
//for all peaks find the length ofits spread: O(n)

//can you do both the steps in single pass?

//peak ke liye minimum 3 eleemnts toh chiaye he
//agar overlap hoga 2 paeks me toh ek element ka he higa na logically because of this he O(n) poora peeche tak traverse nahi karna padeg a
#include <iostream>
#include <vector>
using namespace std;

int longestPeak(vector<int>& A) {
    if(A.size()<3)
        return 0;

    int longestPeakLength = 0;
    int i = 1;

    while(i<A.size() - 1) {
        bool isPeak = (A[i-1] < A[i] && A[i] > A[i+1]);
        if(!isPeak) {
            i++;
            continue;
        }

        /**We need to find the left edge*/
        int leftIndex = i-2; //kyuki i toh peak hai matab uske immediate eala left toh usse chita he hoga na
        while(leftIndex>=0 && A[leftIndex] < A[leftIndex + 1]) {
            leftIndex--;
        }

        /**We need to find the right edge**/
        int rightIndex =i + 2;
        while(rightIndex < A.size() && A[rightIndex] < A[rightIndex - 1]) {
            rightIndex++;
        }
        int currentLength = rightIndex - leftIndex -1;
        longestPeakLength = max(longestPeakLength, currentLength);
         //DOUBT: ideally(length = leftIndex-rigtIndex+1) toh yeh kaise

         //Move the current index to rightIndex for peak search
         i=rightIndex;
    }
    return longestPeakLength;
}
int main() {
    vector<int> nums= {1,2,3,4,0,10,6,5,-1,-3,2,3};
    int peak = longestPeak(nums);
    cout<<"Peak ="<<peak<<endl;

    nums= {2,1,4,7,3,2,5};
    peak = longestPeak(nums);
    cout<<"Peak ="<<peak<<endl;

    nums= {3,3,3,3,3,3};
    peak = longestPeak(nums);
    cout<<"Peak ="<<peak<<endl;

    nums= {2,3};
    peak = longestPeak(nums);
    cout<<"Peak ="<<peak<<endl;

    return 0;
}
