#include<iostream>
#include<vector>

using namespace std;

//Time: O(n)
//Space: O(n)
/*
int trap(vector<int>& height) {
    vector<int> leftMax(height.size(), 0);

    int currentLeftMax = 0;
    for(int i=0; i<height.size(); ++i) {
        leftMax[i] = currentLeftMax;
        currentLeftMax = max(currentLeftMax, height[i]);
    }

    int currentRightMax = 0;
    int total = 0;
    for(int i=height.size()-1; i >=0; --i ) {
        int currentHeight = height[i];
        int minHeight = min(currentRightMax,leftMax[i]);

        if(currentHeight < minHeight) {
            int currentWaterTrap = minHeight - currentHeight;
            total = total + currentWaterTrap;
        }
        currentRightMax = max(currentRightMax,height[i]);
    }
    return total;
}
*/

//Time: O(n)
//Space: O(1)

int trap(vector<int>& height) {
    int left = 0;
    int right = height.size() - 1;
    int total = 0;
    int leftMax = 0, rightMax = 0;
    while(left  < right) {
        leftMax = max(leftMax, height[left]);
        rightMax = max(rightMax, height[right]);

        if(height[left] < height[right]) {
            //currentHeight = height[left]
            int currentWaterTrap = leftMax - height[left];
            total += currentWaterTrap;
            ++left;
        } else {
            //currentHeight = height[right]
            int currentWaterTrap = rightMax - height[right];
            --right;
        }
    }
    return total;
}
int main() {
    vector<int> nums ={0,1,0,2,1,0,1,3,2,1,2,1};

    int res = trap(nums);
    cout<<"Total water trapped = " <<res<<endl;

    return 0;
}
