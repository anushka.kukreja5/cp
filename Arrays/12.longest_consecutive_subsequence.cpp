#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;
pair<int,int> longestConsecutiveSubsequence(vector<int> &nums)
{
    pair<int,int> bestRange;
    int longestLength = 0;
    unordered_map<int,bool> hash;

    //Fill in the hash
    for(int i=0; i<nums.size(); ++i) {
        hash[nums[i]] = false;
    }
    //Looping through evry number and finding the range for it
    for(int i=0;i<nums.size();++i) {
        int currentNum = nums[i];

        if(hash[currentNum]) {
            //currentnum is laready visited!
            continue;
        }
        //currentNum is not visited
        hash[currentNum] = true;

        int currentLength = 1;

        int leftNumber = currentNum - 1;
        int rightNumber = currentNum + 1;

        while(hash.find(leftNumber) != hash.end()) {
            hash[leftNumber]  = true;
            ++currentLength;
            --leftNumber;
        }
        while(hash.find(leftNumber) != hash.end()) {
            hash[leftNumber]  = true;
            ++currentLength;
            ++rightNumber;
        }
        if(currentLength > longestLength) {
            longestLength = currentLength;
            bestRange.first = leftNumber + 1; //Just Check this! Its the most common mistake
            bestRange.second = rightNumber - 1;
        }
    }
    return bestRange;

}
int main() {
    vector<int> nums= {0,1,9,5,6,7,2,3,4,11,12,1,3,45};
    auto range = longestConsecutiveSubsequence(nums);
    cout<<"Longest consecutive susequence is from: "<<range.first<<" to: "<<range.second;

    return 0;
}
