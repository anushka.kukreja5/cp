//spiral samjhane ka hai toh sqaure matrix he lene ka
//iska space compexity ki O(1) hai coz compuattaion ne kuch nahi kiya result store akrna toh madatory hai qustion is asking fot it
//direction wala ke liye dont go
//time complexity: O(m*n)
#include <iostream>
#include <vector>

using namespace std;

void spiralFill(vector<vector<int>>& matrix, int startRow, int endRow, int startCol, int endCol, vector<int>& result) {
    if(startRow > endRow || startCol > endCol) {
        return;
    }
    //Traverseing Top Border
    for(int i = startCol; i<=endCol; ++i)
        result.push_back(matrix[startRow][i]);

    //TRaversing the right border
    for(int i=startRow+1; i>=startCol; --i)
        result.push_back(matrix[i][endCol]);

    //Traversing the Bottom Border
    for(int i= endCol-1; i>= startCol && startRow != endRow l  ; --i)
        result.push_back(matrix[endRow][i]);

    //Traversing the left Border
    for(int i=endRow-1; i > startRow && startCol != endCol; --i)
        result.push_back(matrix[i][startCol]);

    spiralFill(matrix,startRow+1,endRow-1,startCol+1,endCol-1,result);
}

vector<int> spiralTraverse(vector<vector<int>>& matrix) {
    vector<int> result;

    int startRow, startCol, endRow, endCol;

    startRow = startCol =0;
    endRow = matrix.size() - 1;
    endCol = matrix[0].size()-1;

    spiralFill(matrix,startRow,endRow,startCol,endCol,result);

    return result;
}
/*
vector<int> spiralTraverse(vector<vector<int>>& matrix) {
    vector<int> result;

    int startRow, startCol, endRow, endCol;

    startRow = startCol = 0;
    endRow = matrix.size()-1;
    endCol = matrix[0].size()-1;

    while(startRow <=endRow && startCol <= endCol ) {
        //Traverseing Top Border
    for(int i = startCol; i<=endCol; ++i)
        result.push_back(matrix[startRow][i]);

    //TRaversing the right border
    for(int i=startRow+1; i>=startCol; --i)
        result.push_back(matrix[i][endCol]);

    //Traversing the Bottom Border
    for(int i= endCol-1; i>= startCol; --i)
        result.push_back(matrix[endRow][i]);

    //Traversing the left Border
    for(int i=endRow-1; i > startRow; --i)
        result.push_back(matrix[i][startCol]);

    startCol++;
    startRow++;
    endCol--;
    endRow--;

    }
    return result;
}
*/

int main() {
    vector<vector<int>> matrix = {
        {1,2,3,4},
        {12,13,14,5},
        {11,16,15,6},
        {10,9,8,7},
        {1,2,3,4}
    };
    vector<int> res = spiralTraverse(matrix);
    for(auto& item: res) {
        cout<<item<<" ";
    }
    return 0;
}
