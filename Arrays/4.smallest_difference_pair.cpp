//while waale loop ko efficient karne bola toh e=he ek if condition daal diya ki difference 0 aaay toh udar he return isse woh loop ka best case willl be O(1)
#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_map>

using namespace std;

//Time: O(n log n

//Space: O(1))
pair<int,int> smallestDifferencePair(vector<int> nums1, vector <int> nums2) {
    int index1, index2, runningMin =    INT_MAX;
    pair<int,int. result;

    sort(begin(nums1), end(nums1));
    sort(begin(nums2), end(nums2));

    index1 = 0;
    index2 = 0;

    while(index1 < nums1.size() && index2 < nums2.size()) {
        int currentDiff = abs(nums1[index1] -  nums2[index2]);
        if(currentDiff < runningMin) {
            result.first = nums1[index1];
            result.second = nums2[index2];
            runningMin = currentDiff;
        }

        if(nums1[index1] < nums2[index2]) {
            index1++;
        } else {
            index2++;
        }
    }
    return result;
}

int main() {
    vector<int> nums1 {-1,5,10,20,28,3};
    vector <int> nums2 { }
}
