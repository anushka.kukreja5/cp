#include<iostream>
#include<vector>

using namespace std;

int maxSubarray(vector <int> &nums) {
    int sz = nums.size();

    int currSum = nums[0];
    int result = nums[0];
    for(int i=1;i<sz;++i) {
        currSum = max(currSum + nums[i], nums[i]);
        result = max(result, currSum);
    }
    return result;
}

int main() {
    vector<int> nums {-2,1,-3,4,-1,2,1,-5,4};
    int res;
    res = maxSubarray(nums);
    cout << res      ;
    return 0;

}
