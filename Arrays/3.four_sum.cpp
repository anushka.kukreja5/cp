//iska average n^2 hai and worst n^3 hai DOUBT:
#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;
vector<vector<int>> fourNumberSum(vector<int> numbers, int target) {
    vector<vector<int>> quadruplets;
    unordered_map<int,vector<pair<int,int>>> hash;
    for(int i = 1; i < numbers.size()-1; ++i) {
        for(int j = i+1; j <numbers.size(); ++j) {
            int currentSum = numbers[i] + numbers[j];
            int difference =  target - currentSum;
            if(hash.find(difference) != hash.end()) {
                for(auto &pairs: hash[difference]) {
                    vector<int> quad(4,0);
                    quad[0] =  pairs.first;
                    quad[1] = pairs.second;
                    quad[2] = numbers[i];
                    quad[3] = numbers[j];

                    quadruplets.push_back(quad);
                }
            }
        }

        for(int k = 0; k<i; ++k) {
            int currentSum = numbers[i]  + numbers[k];
            pair<int,int> tempPair;
            tempPair.first = numbers[k];
            tempPair.second = numbers[i];

            if(hash.find(currentSum) == hash.end()) {
                hash[currentSum] = vector<pair<int,int>>();
            }
            hash[currentSum].push_back(tempPair);
        }
    }
    return quadruplets;
}
int main() {
    vector<int> nums = {10,2,3,4,5,9,7,8};
    int target = 23;
    vector<vector<int>> res = fourNumberSum(nums,target);
    for(auto& item: res) {
        for(auto &i: item) {
            cout<<i<<" ";
        }
        cout << endl;
    }

    return 0 ;
}
