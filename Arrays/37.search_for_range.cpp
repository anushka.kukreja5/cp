#include<iostream>
#include<vector>
#include<algorithm>
#include<numeric>

using namespace std;
/*
void alteredBinarySearch(vector<int> nums, int target, int low, int high, vector<int> &result, bool goLeft) {
    while(low<= high) {
        int mid = (low+high) / 2;

        if(nums[mid]  < target) {
            low = mid +1;
        }else if(nums[mid]  >target) {
            high = mid - 1;
        } else {
            if(goLeft) {
                if(mid == 0 || nums[mid -1] != target) {
                    result[0] = mid;
                    return;
                } else {
                    high = mid - 1;
                }
            }else {
                if(mid == nums.size()-1 || nums[mid+1] != target) {
                    result[1] = mid;
                    return;
                } else {
                    low = mid + 1;
                }
            }
        }
    }
}
vector<int> searchForRange(vector<int> nums,int target) {
    vector<int> result {-1,-1};
    alteredBinarySearch(nums,target,0,nums.size()-1,result,true);
    alteredBinarySearch(nums,target,0,nums.size()-1,result,false);
    return result;
}
*/
vector<int> searchForRange(vector<int> nums,int target) {
    vector<int> result {-1,-1};
    if(binary_search(begin(nums),end(nums),target)) {
        auto[lower,upper] = equal_range(begin(nums),end(nums),target);
        result[0] = distance(begin(nums),lower);
        result[1] = distance(begin(nums),upper)-1;
    }
    return result;
}

int main() {
    vector<int> nums{10,10,20,20,20,20,20,20,50,50,100};
    auto result = searchForRange(nums,20);
    cout<<result[0]<<" to "<<result[1]<<endl;
    return 0;

}
