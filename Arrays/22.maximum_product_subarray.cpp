#include<iostream>
#include<vector>
#include<tuple>
#include<algorithm>

using namespace std;

int maxProduct(vector<int>& nums) {
    int result = INT_MIN;

    int currentMin = 1;
    int currentMax = 1;

    for(const auto num: nums) {
        tie(currentMin, currentMax) = pair(min({num, num * currentMax, num *currentMin}),
                                           max({num,num*currentMax, num*currentMin}));

       result = max(result, currentMax);
    }

    return result;

}
int main() {
    vector<int> nums {2,3,-2,4,-1};
    int res = maxProduct(nums);
    cout << res;
    return 0;
}

