#include<vector>
#include<list>
#include<iostream>

using namespace std;

vector<int> slidingWindowMaximum(vector<int> nums,int k) {
    vector<int> maxes;
    list<int> window;

    for(int i=0;i<k;++i) {
        while(!window.empty() && nums[i] >= nums[window.back()]) {
            window.pop_back();
        }
        window.push_back(i);
    }

    for(int i=k;i<nums.size();++i) {
        maxes.push_back(nums[window.front()]);

        if(window.front() == (i-k)) {
            window.pop_front();
        }

        while(!window.empty() && nums[i] >= nums[window.back()]) {
            window.pop_back();
        }
        window.push_back(i);
    }
    maxes.push_back(nums[window.front()]);
    return maxes;
}

int main() {
    vector<int> nums= {10,4,2,11,3,15,12,8,7,9,21,24};
    int k= 3;
    vector<int> res= slidingWindowMaximum(nums,k);

    for(int item: nums) {
        cout<<item<<" ";
    }
    cout<<endl;
    for(int item: res) {
        cout<<item<<" ";
    }
    return 0;
}
