#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>

using namespace std;

//Time: O(n
;

vector <int> twoNumberSumUsingHashTable (vector <int> numbers, int target) {
    vector <int> result;
    unordered_map<int, int> hash;
    for(int i =0; i <numbers.size(); ++i) {
        int currentNumber=  numbers[i];
        int secondNumber = target - currentNumber;

        //Search the second number
        if(hash.find(secondNumber) != hash.end()) {
            result.push_back(currentNumber);
            result.push_back(secondNumber);
            return result;
        }
        //If not found then insert the current number in Hash, so that it can be later searched!
        hash[currentNumber] = i;
    }
    return result;
}

//Time: O(n log n)
//Space: O(1)
vector<int> twoNumberSumUsingSorting(vector<int> numbers, int target) {
    int left, right;
    vector <int> result;

    sort(begin(numbers),end(numbers));

    left = 0 ;
    right = numbers.size()-1;

    while(left < right) {
        int testSum = numbers[left] + numbers[right];
        if(testSum == target) {
            result.push_back(numbers[left]);
            result.push_back(numbers[right]);
            return result;
        }else if(testSum  < target) {
            left++;
        }else {
            right--;
        }

    }
    return result;
}
int main() {
    vector<int> nums {3,5,-4,8,11,1,-1,6};
    int target = 10;

    vector<int> res = twoNumberSumUsingHashTable(nums,target);
    for(auto &item: res) {
        cout << item <<" ";

    }
    cout << endl;

    res = twoNumberSumUsingSorting(nums, target);
    for(auto &item: res) {
        cout << item <<" ";

    }
    cout << endl;
    return 0;


}
