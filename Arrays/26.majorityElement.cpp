
/* majority element = element that appears more than n/2 times where n= size of array
yeh program me array sorted hai and it returns bool
*/#include<iostream>
#include<vector>
#include<math.h>

using namespace std;

bool majorityElement (vector<int> arr) {
    int sz = arr.size();
    int jump = ceil((sz/2))-1;

    for(int i=0;i<=sz/2;i++) {
        if(arr[i] == arr[i+jump])
            return true;
    }
    return false;
}
int main() {
    vector<int> nums {1,2,2,2,2,3,3,3};
    auto res = majorityElement(nums);
    cout << res << endl;
    return 0;
}
