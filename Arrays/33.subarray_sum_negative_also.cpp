#include<vector>
#include<unordered_map>
#include <iostream>
//also find teh subarray whose sum =0 ;
using namespace std;
//Time: O(n)
//Space: O(n)

bool subarraySums(vector<int> nums, int x) {
    unordered_map<int,int> hashmap;
    int sum = 0;
    for(int i=0; i<nums.size();++i) {
        sum = sum + nums[i];
        int difference = sum - x;


        //difference ==0 condition is if exact sum mil gaya

        if(difference == 0 || hashmap.find(difference) != hashmap.end()) {
            return true;
        }
        hashmap[sum] = i;
    }
    return false;
}
int main() {
    vector<int> nums = {8,5,-2,3,4,-6,70};
    int x = 10;
    bool res = subarraySums(nums,x);
    cout<<boolalpha;
    cout<<res<<endl;
    return 0;
}
