//ek number akela kabhi unsorted nahi hoga
//minimum  1 set of numbers  chaiye uska min ko uski posiiton pe aur max ka place dhundh ko uske udar
#include <iostream>
#include <vector>
using namespace std;

bool isOutOfOrder(int index, int currentNumber, vector<int> nums) {
    if(index == 0) {
        return currentNumber > nums[index+1];
    } else if(index == nums.size() - 1) {
        return currentNumber < nums[index-1];
     }

     return currentNumber > nums[index+1] || currentNumber < nums[index-1];
}
pair<int,int> shortestUnortedContinuousSubarray(vector<int>& nums) {
    /*
    //bestcase
    if(is_sorted(nums.begin(), nums.end())){
            return make_pair(-1,-1);
    }
    */

    int minOutOfOrder = INT_MAX;
    int maxOutOfOrder = INT_MIN;

    for(int i=0;i <nums.size()-1;++i) {
        int currentNumber = nums[i];
        if(isOutOfOrder(i,currentNumber,nums)) {
            minOutOfOrder = min(minOutOfOrder, currentNumber);
            maxOutOfOrder = max(maxOutOfOrder, currentNumber);
        }
    }

    if(minOutOfOrder == INT_MAX) {
        return make_pair(-1,-1);
    }
    int subArrayLeftIndex = 0;
    while(minOutOfOrder >= nums[subArrayLeftIndex]) {
        subArrayLeftIndex++;
    }
    int subArrayRightIndex = nums.size() -1;
    while(maxOutOfOrder <= nums[subArrayRightIndex])
        subArrayRightIndex--;

    return make_pair(subArrayLeftIndex, subArrayRightIndex);
}

int main() {
    vector<int> nums = {1,2,3,7,10,11,7,12,6,7,16,18,19};
    pair<int,int> unsortedArrayIndex = shortestUnortedContinuousSubarray(nums);
    cout<<"Left: "<<unsortedArrayIndex.first<<endl;
    cout<<"Right: "<<unsortedArrayIndex.second<<endl;

    nums= {1,2,3,4,5,6,7,8};
    unsortedArrayIndex = shortestUnortedContinuousSubarray(nums);
    cout<<"Left: "<<unsortedArrayIndex.first<<endl;
    cout<<"Right: "<<unsortedArrayIndex.second<<endl;

    nums= {2,6,4,8,10,9,15};
    unsortedArrayIndex = shortestUnortedContinuousSubarray(nums);
    cout<<"Left: "<<unsortedArrayIndex.first<<endl;
    cout<<"Right: "<<unsortedArrayIndex.second<<endl;

    return 0;
}
