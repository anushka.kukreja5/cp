#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

int minCandies(vector<int>& rewards) {
    vector<int> candies(rewards.size(),1);
    int sz = rewards.size();

    //Expanding towards right!
    for(int = 1; i < sz; ++i) {
        if(rewards[i] > rewards[i-1]) {
            candies[i] = candies[i-1]+1;
        }
    }

    //Expanding towards left
    for(int i =sz-2; i>=0; --i) {
        if(rewards[i] > rewards[i+1]) {
            candies[i] = max(candies[i], candies[i+1]+1);
        }
    }

    int initial_sum = 0;
    return accumulate(begin(candies),end(candies),initial_sum);
}
int main() {

}
