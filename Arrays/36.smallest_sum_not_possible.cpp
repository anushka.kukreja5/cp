#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int smallestSumNotPossible(vector<int> nums) {
    int sum = 1;

    sort(nums.begin(), nums.end());

    for(int i=0; i<nums.size(); ++i) {
        if(sum < nums[i]) {
            return sum;
        }
        sum = sum +nums[i];
    }
    return sum;
}
int main() {
    vector<int> nums {4,13,2,3,1};
    auto result = smallestSumNotPossible(nums);
    cout<<result<<endl;

    return 0;

}
