#include<iostream>
#include<vector>
#include<numeric>
using namespace std;

int equilibriumIndex(vector<int> nums) {
    int sum =0 ;
    sum = accumulate(begin(nums),end(nums),0);

    int leftSum = 0;
    for(int i=0; i<nums.size();++i) {
        leftSum += nums[i];
        if(leftSum == (sum-leftSum)) {
            return i;
        }
    }
    return -1;
}
int main() {
    vector<int> nums = {10,5,15,3,4,21,2};
    auto index = equilibriumIndex(nums);
    cout<<index<<endl;
    return 0;
}
