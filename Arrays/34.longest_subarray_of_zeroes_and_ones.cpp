#include<iostream>
#include<unordered_map>
#include<vector>

using namespace std;

int longestSubarrayOfZerosAndOnes(vector<int> nums) {
    unordered_map<int,int> hashmap;

    for(int i=0; i< nums.size();++i) {
        nums[i] = nums[i] ==0? -1 :1;

    }
    hashmap[nums[0]] = 0;
    int sum = nums[0];
    int lb = -1;
    int ub = -1;

    int sizeOfLargestSubarray = 0;

    for(int i=1; i < nums.size(); ++i) {
        sum = sum+ nums[i];

        //Edge case where the sum results in 0 itself
        if(sum  ==0 ) {
            int tempSize  =i;
            if(tempSize  >sizeOfLargestSubarray) {
                sizeOfLargestSubarray = tempSize;
                lb = 0;
                ub = i;
            }
        }
        if(hashmap.find(sum) != hashmap.end()) {
            int idx = hashmap[sum];


            if(i - (idx+1)  >sizeOfLargestSubarray) {
                lb = idx + 1;
                ub = i ;
                sizeOfLargestSubarray = ub -lb;
            }
        }else {
            hashmap[sum] = i;
        }
    }
    return (sizeOfLargestSubarray  +1)/2;
}
int main() {
    vector<int> nums= {1,1,0,0,0,1};
    int res = longestSubarrayOfZerosAndOnes(nums);
    cout << res;
    return 0;
}
