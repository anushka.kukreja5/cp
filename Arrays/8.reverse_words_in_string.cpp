//A function that reverses a  whole sentence character by character
#include <regex>
#include <iostream>
void StrRev(std:: string &_str, int start_rev, int end_rev)
 {

     while(start_rev < end_rev) {
        char temp = _str[start_rev];
        _str[start_rev] = _str[end_rev];
        _str[end_rev] = temp;
        start_rev +=1;
        end_rev -=1;
     }
 }
 //apne banaye huye function ko capital se start kiya jaise nterviewer ko expalin karne me confusion nahi ho ki kaunsa mera function hai kaunsa buit in-function hai
 std::string ReverseWords(std::string sentence) {
    sentence = std::regex_replace(sentence,std::regex("^ +| +$| ( ) +"),"$1");
    int strLen = sentence.size();
    StrRev(sentence,0,strLen -1);

    int start =0, end = 0;

    while(start < strLen)
    {
        while(end< strLen && sentence[end] != ' ')
            end +=1;
        StrRev(sentence,start,end -1);
        start = end + 1;
        end += 1;
    }
    return sentence;
 }
 //Driver code
 int main() {
    std::vector<std::string> stringToReverse = {
        " Hello World ",
        "We love java",
        "The quick brown fox jumped over the lazy dog",
        "Hey",
        "To be or not to be",
        "AAAA",
        " Hello     World "};
    for(int i = 0; i < stringToReverse.size(); i++)
    {
        std::cout << (i+1) <<".\tActual string:\t\t" << stringToReverse[i] <<std::endl;
        std::string result = ReverseWords(stringToReverse[i]);
        std::cout << "\tReversed string:\t" << result << std::endl;
        std::cout << std::string (100,'-') << std::endl;
    }
 }
