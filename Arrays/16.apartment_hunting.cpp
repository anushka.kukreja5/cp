
#include <vector>
#include <iostream>

int distanceBetween(int index1, int index2) {
    return (abs(index1-index2));
}

int getIndexOfMinValue(vector<int> a) {
    int minValue =  INT_MAX;
    int minIndex = -1;

    for(int i=0; i<a.size(); ++i) {
        if(minValue > a[i]) {
            minValue = a[i];
            minIndex = i;
        }
    }

    return minIndex;
}

vector<int> getMaxDistancesOfServuceAtEachBlock(unordered_map<string,vector<int>> minDistances, vector<string> reqs) {
    int numBlocks = minDistances[reqs[0]].size();
    vector<int> maxDistancesOfServiceAtBlocks(numBlocks,INT_MIN);

    for(int i =0; i < numBlocks; ++i) {
        int maxDistance =   INT_MIN;
        for(string requirement: reqs) {
            maxDistance = max(maxDistance, minDistance[requirement][i]);
        }
        maxDistanceOfServiceAtBlocks[i] = maxDistance;
    }
    return maxDistanceOfServiceAtBlocks;
}

vector<int> getMinimumDistances(vector<unordered_map<string,bool>>, string requirement) {
    vector<int> distances(blocks.size(),INT_MAX);
    int nearestRequirementIndex = INT_MAX;
    for(int i=0; i<blocks.size(); ++i) {
        if(blocks[i][requirement]) {
            nearestRequirementIndex = i
        }
        distances[i] = distanceBetween(i,nearestRequirementIndex);
    }

    for(int i=blocks.size()-1; i  >=0; --i) {
        if(blocks[i][requirement]) {
            nearestRequirementIndex = i;
        }
        distances[i] = min(distance,distanceBetween(i,nearestRequirementIndex));
    }
    return distances;
}

int apartmentHunting (vector<unordered_map<string,bool>> blocks, vector<string> reqs) {
    unordered_map<string,vector<int>> minDistances;
    vector<int> maxDistancesOfServicesAtBlock(blocks.size(),INT_MIN);

    for(string requirements: reqs) {
        minDistances[requirement] = getMinimumDistances(blocks, requirement);
    }
    maxDistancesOfServicesAtBlocks = getMaxDistancesOfServiceAtEachBlock(minDistances,reqs);

    return getIndexOfMinValue(maxDistancesOfServicesAtBlocks);
}

int main() {
    vector<unordered_map<string,bool>> blocks = {

        {{
    }
    return 0;
}
