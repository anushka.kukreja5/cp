//ZigZag traversal
//there are 2 directions:
/*
1.diagonal up: row-1, col+1
2.diagonal down: row+1, col -1;

jab top row me hai

you will go to rigt only wehn you are at top row an dyou want to change to next column
but when you are at top row and last colum you wont follow the above column(you wont go right) instead uyou will go down and change the direction to diagonal down

1st col -> down
1st row -> right
last col -> down
last row -. right
 //time and space complexitu: O(m*n)
 //zigzag traversal is used in jpeg compression
 discrete cosine transform is a type of fourier
*/
#include <iostream>
#include <vector>
 using namespace std;

 vector<int> zigzagTraverse(vector<vector<int>> matrix) {
    vector<int> result;
    bool goingDiagDown = true;
    int row,col;
    row = 0;
    col = 0;

    while(row < matrix.size() && col< matrix[0].size()) {
        //continue our zigzag traversal

        //1.Append the current value in the result
        result.push_back(matrix[row][col]);

        //2.Take the decision according to the current direction
        if(goingDiagDown) {
            //we are going down
            if(col == 0 || row ==  matrix.size()-1) {
                //Wither reached ;ast row or 1st col
                goingDiagDown = false;
                if(row == matrix.size() - 1) {
                    ++col;
                } else {
                    ++row;
                }
            } else {
            //we are going diagonally down
                ++row;
                --col;
            }
        } else {
            //We are going up
            if(row == 0 || col == matrix[0].size() -1) {
                goingDiagDown = true;
                if( col == matrix[0].size() - 1) {
                    ++row;
                } else {
                    ++col;
                }
            } else {
            //We are going diagonally up
                --row;
                ++col;
            }
        }
    }
    return result;
 }
 int main() {
    vector<vector<int>> matrix ={
        {1,3,4,10},
        {2,5,9,11},
        {6,8,12,15},
        {7,13,14,16}
    };

    vector<int> res = zigzagTraverse(matrix);
    cout<<"Result: ";
    for(auto &item: res) {
        cout<<item<<" ";

    }
    cout<<endl;
    return 0;
 }
