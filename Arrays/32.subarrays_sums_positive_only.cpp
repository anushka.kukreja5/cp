#include<iostream>
#include<vector>

using namespace std;

bool subarraySums(vector<int> nums, int x) {
    if(nums.size() < 1) {
        return false;
    }

    int low,high,sum;
    low = 0;
    sum = nums[0];

    for(high =1 ;high<=nums.size(); ++high) {
        while(sum  >x && low < high -1) {
            sum = sum - nums[low++];
        }
        if(sum == x) {
            return true;
        }

        //this has to be after the above condition
        if(high  <nums.size()) {
            sum = sum + nums[high];
        }

    }
    return false;
}

int main() {
    vector<int> nums = {5,4,6,7,9,8,3,1,2};
    int x=21;
    cout<<boolalpha;
    auto res =  subarraySums(nums,x);
    cout<<res<<endl;
    return 0;

}
